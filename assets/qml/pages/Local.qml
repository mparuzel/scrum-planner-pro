import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Page {
    objectName: "LocalSession"
    id: localPage
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        layout: StackLayout {            
        }

        Container {
            Header {
                title: "Scrum Planner Pro"
                subtitle: "Local Session"
            }
        }
        
        DeckDropDown {
            id: deckPicker
            
            onDeckSelected: {
                // Enable deck actions
                deleteDeckBtn.enabled = true;
                editDeckBtn.enabled = true;
                shareDeckBtn.enabled = true;
            }
            
            onNoDeckSelected: {
                // Disable deck actions
                deleteDeckBtn.enabled = false;
                editDeckBtn.enabled = false;
                shareDeckBtn.enabled = false;
            }
        }        

        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill

            DeckView {
                id: deckView

                onTriggered: {
                    var selectedItem = dataModel.data(indexPath);
                    var page = displayCardPage.createObject();

                    // Set text of full-screen card
                    page.text = selectedItem.text;

                    // Show the next pane with the card
                    navigationPane.push(page);
                }
                
                attachedObjects: [
                    ComponentDefinition {
                        id: displayCardPage
                        source: "asset:///qml/pages/DisplayCard.qml"
                    }
                ]
            }
        }

    }

    // ==========================================================================================================
    // ==========================================================================================================
    
    function onDeckSelected(deckName, cardList) {
        deckView.loadDeck();
    }
    
    function onDeckDoesNotExist(deckName) {
        errToast.body = "Deck '" + deckname + "' does not exist";
        errToast.show();
    }
    
    function onInvokeError(err) {
        errToast.body = err;
        errToast.show();
    }

    function onInitDeckList() {
        var toSelect = DeckRepository.activeDeck;
        
        // Clear the drop down before readding items
        deckPicker.removeAll();
        deckPicker.populate(DeckRepository.availableDecks);
        deckPicker.select(toSelect);
    }

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        // Init signals
        DeckRepository.deckSelected.connect(onDeckSelected);
        DeckRepository.deckDoesNotExist.connect(onDeckDoesNotExist);
        DeckRepository.deckListUpdated.connect(onInitDeckList);
        InvokeManager.invokeError.connect(onInvokeError);
        
        // Init state
        onInitDeckList();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    attachedObjects: [
        SystemToast {
            id: errToast
            body: ""
        }
    ]

    // ==========================================================================================================
    // ==========================================================================================================

    actions: [
        ActionItem {
            id: createNewDeckBtn
            title: qsTr("New Deck")
            ActionBar.placement: ActionBarPlacement.InOverflow
            imageSource: "asset:///images/add_deck.png"
            
            onTriggered: {
                // Move on to the next page
                var page = newDeckPage.createObject();
                page.newDeck();                
                page.open();
            }

            attachedObjects: ComponentDefinition {
                id: newDeckPage
                source: "asset:///qml/sheets/EditDeck.qml"
            }
        },
        ActionItem {
            id: editDeckBtn
            title: qsTr("Edit Deck")
            ActionBar.placement: ActionBarPlacement.InOverflow
            imageSource: "asset:///images/edit_deck.png"
            
            onTriggered: {
                // Move on to the next page
                var page = editDeckPage.createObject();
                page.editDeck(DeckRepository.activeDeck, DeckRepository.activeDeckCards);
                page.open();
            }
            
            onEnabledChanged: {
                console.log("I'm selected: " + enabled);
            }

            attachedObjects: ComponentDefinition {
                id: editDeckPage
                source: "asset:///qml/sheets/EditDeck.qml"
            }
        },
        ActionItem {
            id: deleteDeckBtn
            title: qsTr("Delete Deck")
            ActionBar.placement: ActionBarPlacement.InOverflow
            imageSource: "asset:///images/delete_deck.png"

            onTriggered: {
                if (deckPicker.selectedItem != null) {
                    // Show prompt to delete deck
                    removeDeckDialog.body = "Are you sure you want '" + deckPicker.selectedItem.text + "' deleted?";
                    removeDeckDialog.show();
                }
            }
            
            attachedObjects: [
                SystemDialog {
                    id: removeDeckDialog
                    title: "Delete Deck"
                    body: ""

                    onFinished: {
                        if (removeDeckDialog.result == SystemUiResult.CancelButtonSelection) {
                            return;
                        }

                        if (deckPicker.selectedItem != null) {
                            // Delete the deck
                            DeckRepository.deleteDeck(deckPicker.selectedItem.text);
                        }
                    }
                }
            ]
        },
        InvokeActionItem {
            id: shareDeckBtn
            title: qsTr("Share Deck")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/share.png"

            query {
                mimeType: "application/xml"
                invokeActionId: "bb.action.SHARE"
                uri: "file://" + DeckRepository.getActiveDeckShareFilePath()
            }

            onTriggered: {
                data = "file://" + DeckRepository.getActiveDeckShareFilePath();
            }
        }
    ]

    // ==========================================================================================================
    // ==========================================================================================================

    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: "Back"
            imageSource: "asset:///images/back.png"

            onTriggered: {
                navigationPane.pop();
            }
        }
    }

}
