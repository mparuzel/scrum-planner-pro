import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Page {
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        layout: DockLayout {            
        }

        Header {
            title: "Scrum Planner Pro"
            subtitle: "Local Session"
        }

        CardFace {
            id: displayCard
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            implicitLayoutAnimationsEnabled: false
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

    property alias card: displayCard
    property alias text: displayCard.text;

    // ==========================================================================================================
    // ==========================================================================================================

	onCreationCompleted: {
	    // Display card in full view
	    displayCard.setThumb(false);
	}

}
