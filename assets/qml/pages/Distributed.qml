import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Page {
    Container {       
        Header {
            title: "Scrum Planner Pro"
            subtitle: "Join Distributed Session"
        }

        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            topPadding: 5.0
            
            Label {
                text: "Enter in the <b><i>IP Address</i></b> of the host device you want to connect to. Otherwise, you may host your own session by presing the <b><i>Host</i></b> button."
                textStyle.fontSize: FontSize.XXSmall
                horizontalAlignment: HorizontalAlignment.Center
                textFormat: TextFormat.Html
                multiline: true
                textStyle.textAlign: TextAlign.Center
            }
        }

        Divider {
        }

		UserName {
		    id: userName
            topPadding: 20
        }

        IPAddress{
            id: ipAddress
        	topPadding: 20
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

	property bool isIPValid: true;
	property bool isNameValid: true;

    // ==========================================================================================================
    // ==========================================================================================================
    
    function setHost(ip, port) {
        ipAddress.ip = ip;
        ipAddress.port = port;
    }
	
	function onIPAddressChange(isValid) {
	    isIPValid = isValid;

		// Enable connect button if all is valid
        connectBtn.enabled = (isNameValid && isIPValid);
    }
	
	function onUserNameChange(isValid) {
	    isNameValid = isValid;

        // Enable connect button if all is valid
        connectBtn.enabled = (isNameValid && isIPValid);
    }

    // ==========================================================================================================
    // ==========================================================================================================
    
    onCreationCompleted: {
        userName.updateValidation.connect(onIPAddressChange);
        ipAddress.updateValidation.connect(onUserNameChange);
        
        userName.name = SessionSettings.getLastUserName();
        ipAddress.ip = SessionSettings.getLastHostIP();
        ipAddress.port = SessionSettings.getLastPort();
    }

    // ==========================================================================================================
    // ==========================================================================================================
    actions: [
        ActionItem {
            id: connectBtn
            title: "Connect"
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/connect_session.png"
            
            onTriggered: {
                var page = connectPage.createObject();
                
                // Save session details
                SessionSettings.setHost(ipAddress.ip, ipAddress.port);
                SessionSettings.setUser(userName.name);                

                // Move on to the next page and start the connection
                page.open();
                page.startConnect(ipAddress.ip, ipAddress.port, userName.name);
            }

            attachedObjects: ComponentDefinition {
                id: connectPage
                source: "asset:///qml/sheets/Connect.qml"
            }
        },
        ActionItem {
            id: hostBtn
            title: "Host"
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/host_session.png"
            onTriggered: {
                var page = hostPage.createObject();
                navigationPane.push(page);
            }

            attachedObjects: ComponentDefinition {
                id: hostPage
                source: "asset:///qml/pages/Host.qml"
            }
        }
    ]

    // ==========================================================================================================
    // ==========================================================================================================

    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: "Back"
            imageSource: "asset:///images/back.png"
            onTriggered: {
                navigationPane.pop();
            }
        }
    }
}
