// Navigation pane project template
import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

NavigationPane {
    id: navigationPane
    peekEnabled: false
    
    Menu.definition: PullDownMenu {
        id: appMenu
    }

    // ==========================================================================================================
    // ==========================================================================================================

    Page {
        Container {
            layout: StackLayout {
            }
            
            Header {
                title: qsTr("Scrum Planner Pro")
                subtitle: qsTr("v1.1")
            }
            
            ImageView {
                imageSource: "asset:///images/title.png"
                scalingMethod: ScalingMethod.None
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                translationY: 150.0
            }
        }

        actions: [
            ActionItem {
                id: startHosting
                title: qsTr("Start")
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/deck.png"

                onTriggered: {
                    var page = localPage.createObject();
                    navigationPane.push(page);
                }
                
                attachedObjects: ComponentDefinition {
                    id: localPage
                    source: "asset:///qml/pages/Local.qml"
                }
            },
            ActionItem {
                id: beginSession
                title: qsTr("Distributed")
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/distributed_session.png"

                onTriggered: {
                    var page = distributedPage.createObject();
                    navigationPane.push(page);
                }

                attachedObjects: ComponentDefinition {
                    id: distributedPage
                    source: "asset:///qml/pages/Distributed.qml"
                }
            }
        ]
    }

    // ==========================================================================================================
    // ==========================================================================================================

	property variant deckName;
	property variant deckCards;
	property variant ipAddr;

    // ==========================================================================================================
    // ==========================================================================================================

    function onDeckImport(name, cards) {
	    deckName = name;
	    deckCards = cards;
	    
	    // Create a prompt
	    if (DeckRepository.isDeck(name)) {
            importDeckDialog.body = "The deck '" + name + "' already exists. Would you like to import and overwrite it?";
        } else {
            importDeckDialog.body = "Would you like to import the '" + name + "' deck into your Scrum Planner Pro application?";
        }
	    
	    // Prompt user
        importDeckDialog.show();
    }
	
	function onServerInfo(ip) {
        ipAddr = ip;
        
        // Prompt User
        connectToHostDialog.body = "Do you want to connect to " + ip + "?";
        connectToHostDialog.show();
    }

	function onInvokeError(err) {
        errToast.body = err;
        errToast.show();
    }
	
	function onInvalidDeck(name, cards) {
        errToast.body = "Invalid deck '" + name + "', cannot import.";
        errToast.show();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    onPopTransitionEnded: {
        page.destroy();
    }
    
    onCreationCompleted: {
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.DisplayPortrait;

		// Connect signals
        InvokeManager.invokedDeck.connect(onDeckImport);
        InvokeManager.invokedServerInfo.connect(onServerInfo);
        InvokeManager.invokeError.connect(onInvokeError);
        DeckRepository.invalidDeck.connect(onInvalidDeck);
        
        // Init files for sharing
        // Get the leading IP on wifi
        var ip = NetworkController.getBestIP();
        
        // Create files for sharing
        ServerRepository.getServerFilePath(ip);
        DeckRepository.getActiveDeckShareFilePath();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    attachedObjects: [
        SystemToast {
            id: errToast
            body: ""
        },
        SystemDialog {
            id: importDeckDialog
            title: "Import Deck"
            body: ""
            modality: SystemUiModality.Application
            confirmButton.label: "Yes"
            cancelButton.label: "No"

            onFinished: {
                // Do nothing on cancel
                if (importDeckDialog.result == SystemUiResult.CancelButtonSelection) {
                    return;
                }
                
                // Otherwise, import deck
                DeckRepository.addDeck(navigationPane.deckName, navigationPane.deckCards, true);
                
                // Notify the user
                if (DeckRepository.isDeck(navigationPane.deckName))
                {
                    errToast.body = "Import Successful.";
                    errToast.show();
                }
            }
        },
        SystemDialog {
            id: connectToHostDialog
            title: "Distributed Session"
            body: ""
            modality: SystemUiModality.Application
            confirmButton.label: "Yes"
            cancelButton.label: "No"

            onFinished: {
                var page = distributedPage.createObject();
                
                // Set IP
                page.setHost(ipAddr);                
                
                navigationPane.push(page);
            }
        }
    ]
    
}
