import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Page {
    Container {
        Header {
            title: qsTr("Scrum Planner Pro")
            subtitle: qsTr("Host Distributed Session")
        }
        
        Container {
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            topPadding: 10.0
            
            Label {
                id: hostIPStr
                text: "167.154.111.153"
                textStyle.fontSize: FontSize.XXLarge
                textStyle.fontWeight: FontWeight.Bold
                textStyle.textAlign: TextAlign.Center
                horizontalAlignment: HorizontalAlignment.Center
            }
        }

        Divider {
        }

        Container {
            horizontalAlignment: HorizontalAlignment.Fill

            Label {
                id: infoBarTxt
                text: qsTr("Inform your collaborators to connect to the <b><i>IP Address</i></b> listed above (wifi only). You can also inform them through Email or BBM using the <b><i>Share</i></b> button.")
                textStyle.fontSize: FontSize.XXSmall
                horizontalAlignment: HorizontalAlignment.Center
                textFormat: TextFormat.Html
                multiline: true
                textStyle.textAlign: TextAlign.Center
            }
        }

        Divider {
        }

        UserName {
            id: nameBox
        }

        Divider {
        }
        
        ScrollableStatus {
            id: statusBox
        }

    }

    // ==========================================================================================================
    // ==========================================================================================================

    property bool isRunning: false;
    property bool isConnected: false;
    property bool isNameValid: false;
    property bool isHostIPValid: false;

    // ==========================================================================================================
    // ==========================================================================================================

    // Update status
    function onUpdateStatus(str) {
        statusBox.add(str);
    }

    function onError(str) {
        statusBox.add(str)
    }

    function onServerRunning() {
        isRunning = true;
        
        var ip = hostIPStr.text;
        var port = nameBox.port;
        var name = nameBox.name;
        
        // Connect to local host
        NetworkController.connect(ip, port, name);

        // Update UI State
        onStateChanged();
    }

    function onServerStopped() {
        isRunning = false;

        // Update UI State
        onStateChanged();
    }
    
    function onConnected() {
        isConnected = true;

        // Update UI State
        onStateChanged();
    }
    
    function onDisconnected() {
        isConnected = false;

        // Update UI State
        onStateChanged();
    }
    
    function onUserNameChanged(isValid) {
        isNameValid = isValid;

        // Update UI State
        onStateChanged();
    }
    
    function onStateChanged() {
        startHostingBtn.enabled = (isNameValid && isHostIPValid && !isRunning);
        startNetSessionBtn.enabled = (isRunning && isConnected);
        shareIPBtn.enabled = (isHostIPValid);
        
        backBtn.imageSource = isRunning ? "asset:///images/cancel.png" : "asset:///images/back.png";
    }

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        // Get the leading IP on wifi
        var ip = NetworkController.getBestIP();

        // Create files for sharing
        ServerRepository.getServerFilePath(ip);
        
        // Hook up signals and slots
        NetworkController.error.connect(onError);
        NetworkController.info.connect(onUpdateStatus);
        NetworkController.serverStarted.connect(onServerRunning);
        NetworkController.serverStopped.connect(onServerStopped);
        NetworkController.connected.connect(onConnected);
        NetworkController.disconnected.connect(onDisconnected);
        nameBox.updateValidation.connect(onUserNameChanged);
        
        // Show Port Number beside user name box
        nameBox.addPortNumber();
        nameBox.name = SessionSettings.getLastUserName();

        if (ip != null && ip.length > 0) {
            onUpdateStatus("First available IP: " + ip);

            hostIPStr.text = ip;
            isHostIPValid = true;
        } else {
            onUpdateStatus("Cannot start hosting; No external IPs are found.");

            hostIPStr.text = "Cannot Find IP";
            isHostIPValid = false;
        }
        
        // Update UI
        onStateChanged();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            id: backBtn
            title: "Back"
            imageSource: "asset:///images/back.png"
            
            onTriggered: {
                if (isRunning) {
                    // Prompt user to stop hosting
                    backDialog.show();
                } else {
                    // Go back to previous pane
                    navigationPane.pop();
                }
            }

            attachedObjects: [
                SystemDialog {
                    id: backDialog
                    title: "Cancel Session?"
                    body: "Do you want to end this planning session?"

                    onFinished: {
                        if (backDialog.result == SystemUiResult.CancelButtonSelection) {
                            return;
                        }

                        // Stop hosting the session and boot all users
                        NetworkController.stopHost();

                        // Go back to previous pane
                        navigationPane.pop();
                    }
                }
            ]
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

    actions: [
        ActionItem {
            id: startHostingBtn
            title: qsTr("Start Hosting")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/host_session.png"

            onTriggered: {
                // Start hosting
                NetworkController.startHost(nameBox.port);
                
                // Save User Name Used
                SessionSettings.setUser(nameBox.name);
            }
        },
        ActionItem {
            id: startNetSessionBtn
            title: qsTr("Enter Session")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/forward.png"

            onTriggered: {
                var page = netSessPage.createObject();
                navigationPane.push(page);
            }

            attachedObjects: [
                ComponentDefinition {
                    id: netSessPage
                    source: "asset:///qml/pages/NetSession.qml"
                }
            ]
        },
        InvokeActionItem {
            id: shareIPBtn
            title: qsTr("Share IP")
            ActionBar.placement: ActionBarPlacement.InOverflow
            imageSource: "asset:///images/share.png"

            query {
                mimeType: "filelist/mixed"
                invokeActionId: "bb.action.SHARE"
                uri: "file://" + ServerRepository.getServerFilePath(hostIPStr.text)
            }
            
            onTriggered: {
                //data = "[{ \"uri\": \"" + ServerRepository.getServerFilePath(hostIPStr.text) + "\" }]";
                data = "file://" + ServerRepository.getServerFilePath(hostIPStr.text);
            }
        }
    ]

}
