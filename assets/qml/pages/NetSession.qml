import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Page {
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        layout: StackLayout {
        }

        Header {
            id: header
            title: "Scrum Planner Pro"
            subtitle: "Distributed Session"
        }
        
        // Admin Only
        DeckDropDown {
            id: deckPicker 
            enabled: false
            
            onDeckSelected: {
                // Use the selected Item
                NetworkController.useDeck(deckPicker.selectedItem.text);
            }
        }
        
        DeckView {
            id: deckView

            onTriggered: {
                clearSelection();
                select(indexPath);
                
                console.log(deckView.get(indexPath)["text"]);
                
                NetworkController.vote(deckView.get(indexPath)["text"]);
            }
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

    // Member Properties
    property variant usersPage: userListPage.createObject()
    
    // ==========================================================================================================
    // ==========================================================================================================

    function onDeckSelected(deckName, cardList) {
        deckView.loadDeck();
    }

    function onInitDeckList() {
        var toSelect = DeckRepository.activeDeck;

        // Clear the drop down before readding items
        deckPicker.removeAll();
        deckPicker.populate(DeckRepository.availableDecks);
        deckPicker.select(toSelect);
    }

    function onUserJoined(ip, name) {
        // Show that the user has joined
        infoToast.body = name + " has joined (" + ip + ")";
        infoToast.show();
    }

    function onUserLeft(ip, name) {
        var user = (name == "") ? "User" : name;
        
        // Show user leaving
        infoToast.body = user + " has left (" + ip + ")";
        infoToast.show();
    }

    function onUpdateSelf() {
        // Show user details in header bar
        header.subtitle = NetworkController.getName() + " ("+ NetworkController.getIP() + ")";
    }

    function onUpdateDeck() {
        // Update Deck view
        deckView.loadDeck();
    }

    function onUpdateVotes() {
        var votes = NetworkController.getVotes();        
        var size = 0;
        
        // Determine size of associative aray
        for (var key in votes) {
            if (votes.hasOwnProperty(key)) size++;
        }
        
        // Clear selection if votes were wiped
        if (size == 0) {
            console.log("clearing selection");
            deckView.clearSelection();
        }
    }

    function onShowVotes(ignorePrompt) {
        if (!usersPage.opened) {
            // Ignore if used
            if (ignorePrompt == null) {
                // Prompt user as to why the user screen is being shown
                infoToast.body = "All votes are in!";
                infoToast.show();
            }
            
            // Show User List
            usersPage.open();
        }
    }
    
    function onDisconnected() {
        // Upon disconnect, exit 
        exitPane();
    }
    
    function onError(err) {
        // Popup a toast with the error
        infoToast.body = err;
        infoToast.show();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    function exitPane() {
        // Disconnect UI Signals
        NetworkController.userJoined.disconnect(onUserJoined);
        NetworkController.userLeft.disconnect(onUserLeft);
        NetworkController.updateDeck.disconnect(onUpdateDeck);
        NetworkController.updateVotes.disconnect(onUpdateVotes);
        NetworkController.updateSelf.disconnect(onUpdateSelf);
        NetworkController.showVotes.disconnect(onShowVotes);
        NetworkController.error.disconnect(onError);
        //NetworkController.connected.disconnect(onConnected);
        NetworkController.disconnected.disconnect(onDisconnected);

        // Stop hosting the session and boot all users
        NetworkController.stopHost();
        NetworkController.disconnect();
        
        // Close the users sheet if open
        usersPage.close();

        // Go back to previous pane
        navigationPane.pop();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        // Deck Signals (Changed by Admin)
        DeckRepository.deckSelected.connect(onDeckSelected);
        DeckRepository.deckListUpdated.connect(onInitDeckList);
        
        // UI Signals
        NetworkController.userJoined.connect(onUserJoined);
        NetworkController.userLeft.connect(onUserLeft);
        NetworkController.updateDeck.connect(onUpdateDeck);
        NetworkController.updateVotes.connect(onUpdateVotes);
        NetworkController.updateSelf.connect(onUpdateSelf);
        NetworkController.showVotes.connect(onShowVotes);
        NetworkController.error.connect(onError);
        NetworkController.connected.connect(onConnected);
        NetworkController.disconnected.connect(onDisconnected);
        
        // Display the deck picker for admin only
        if (NetworkController.isAdmin()) {
            deckPicker.enabled = true;
        }
        
        // Init state
        onInitDeckList();
    }

    // ==========================================================================================================
    // ==========================================================================================================

    actions: [
        ActionItem {
            id: viewUsers
            title: qsTr("View Users")
            ActionBar.placement: ActionBarPlacement.OnBar
            imageSource: "asset:///images/users.png"

            onTriggered: {
                onShowVotes(false);
            }
        }
    ]

    // ==========================================================================================================
    // ==========================================================================================================

    attachedObjects: [
        SystemToast {
            id: infoToast
            body: ""
        },
        SystemDialog {
            id: systemPrompt
            title: "Exit Session?"
            body: "Do you want to exit this planning session?"
            
            onFinished: {
                if (systemPrompt.result == SystemUiResult.CancelButtonSelection) {
                    return;
                }
                
                // Go back to previous pane
                exitPane();
            }
        },
        ComponentDefinition {
            id: userListPage
            source: "asset:///qml/sheets/UserList.qml"
        }
    ]

    // ==========================================================================================================
    // ==========================================================================================================

    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: "Back"
            imageSource: "asset:///images/exit.png"

            onTriggered: {
                // Stop hosting and exit
                systemPrompt.show();
            }
        }
    }
}
