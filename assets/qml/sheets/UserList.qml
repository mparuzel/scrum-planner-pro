import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Sheet {
    id: userListSheet

    content: Page {
        id: userListPage

        // ==========================================================================================================
        // ==========================================================================================================

        titleBar: TitleBar {
            id: userListBar
            title: "Users"
            visibility: ChromeVisibility.Visible

            dismissAction: ActionItem {
                title: "Close"
                
                onTriggered: {
                    console.log("closing button -");
                    
                    if (NetworkController.isAdmin()) {
                        var votes = NetworkController.getVotes();
                        var users = NetworkController.getClients();
                        var vSize = 0;
                        var uSize = 0;
                        
                        // Determine number of votes
                        for (var key in votes) {
                            if (votes.hasOwnProperty(key)) vSize++;
                        }
                        
                        // Determine number of users
                        for (var key in users) {
                            if (users.hasOwnProperty(key)) uSize++;
                        }

                        // If equal users to votes, ask if votes should be cleared before leaving
                        if (vSize == uSize) {
                            clearVotesPrompt.show();
                        }
                    }

                    console.log("closing button A");
                    // Hide the Sheet
                    userListSheet.close();
                    console.log("closing button B");
                }
            }
        }

        // ==========================================================================================================
        // ==========================================================================================================

        Container {
            Header {
                id: header
                title: "Scrum Planner Pro"
                subtitle: "User List"
            }

            DeckView {
                id: voteView

                onTriggered: {
                    // Only allow the selection if admin
                    if (NetworkController.isAdmin()) {
                        clearSelection();
                        select(indexPath);

                        // Enable delete button
                        kickUser.enabled = isSelected(indexPath);
                    }
                }
            }
        }

        // ==========================================================================================================
        // ==========================================================================================================

        property string noVoteImage: "asset:///images/waiting_on_vote.png"
        property string votedImage: "asset:///images/voted.png"

        // ==========================================================================================================
        // ==========================================================================================================

        function onUpdateUsers() {
            var users = NetworkController.getClients();
            
            console.log("onUpdateUsers");
            
            // Remove all items from the deck 
            voteView.removeAll();
            
            // Add each item into the list
            for (var ip in users) {
                var name = users[ip].length > 0 ? users[ip] : ip;
                
                console.log("adding user:" + name);
                
                // Add an unknown User to the list
                voteView.updateAt({title: name, 
                                   text: "",
                                   image: noVoteImage,
                                   meta: ip});
            }

            // Associate Votes with the users
            onUpdateVotes();
            
            console.log("Done update users");
        }
        
        function onUpdateVotes() {
            var users = NetworkController.getClients();
            var votes = NetworkController.getVotes();

            console.log("onUpdateVotes");

            for (var ip in users) {
                var votedCard = votes[ip] != null ? votes[ip] : "";
                var votedImg = votes[ip] != null ? votedImage : noVoteImage;
                
                console.log("adding vote: " + votedCard);
                
                // Update entry with chosen card
                voteView.updateAt({meta: ip, 
                                   text: votedCard,
                                   image: votedImg});
            }

            console.log("Done update votes");
        }
        
        function onShowVotes() {
            var votes = NetworkController.getVotes();
            
            console.log("showing votes");

            for (var ip in votes) {
                // Remove the need for an image, showing text
                voteView.updateAt({meta: ip,
                                   image: ""});
            }
        }

        // ==========================================================================================================
        // ==========================================================================================================

        actions: [
            ActionItem {
                id: kickUser
                title: "Kick User"
                enabled: false
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/kick_user.png"

                onTriggered: {
                    var data = voteView.get(voteView.selectedItem);
                    
                    // Make sure user doens't kick themselves
                    if (data["meta"] != NetworkController.getIP()) {
                        kickUserPrompt.body = "Do you want to kick " + data["title"] + "?";
                        kickUserPrompt.show();
                    } else {
                        errToast.body = "Stop trying to kick yourself, silly.";
                        errToast.show();
                    }
                }

                attachedObjects: [
                    SystemDialog {
                        id: kickUserPrompt
                        title: "Kick User"
                        body: ""

                        onFinished: {
                            // Ignore if Cancel button is pushed
                            if (kickUserPrompt.result == SystemUiResult.CancelButtonSelection) {
                                return;
                            }

                            var data = voteView.get(voteView.selectedItem);

                            // Kick the user based on IP
                            NetworkController.boot(data["meta"]);
                        }
                    },
                    SystemToast {
                        id: errToast
                        body: ""
                    }
                ]
            },
            ActionItem {
                id: clearVotes
                title: "Clear Votes"
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/clear_votes.png"

                onTriggered: {
                    clearVotesPrompt.show();
                }

                attachedObjects: [
                    SystemDialog {
                        id: clearVotesPrompt
                        title: "Clear Votes"
                        body: "Do you want to clear everyone's votes from the session?"
                        confirmButton.label: "Yes"
                        cancelButton.label: "No"

                        onFinished: {
                            // Ignore if Cancel button is pushed
                            if (clearVotesPrompt.result == SystemUiResult.CancelButtonSelection) {
                                return;
                            }

                            // Clear the votes from the session
                            NetworkController.clearVotes();
                        }
                    }
                ]
            }
        ] // Actions
    } // Page

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        // UI Signals
        NetworkController.updateUsers.connect(userListPage.onUpdateUsers);
        NetworkController.updateVotes.connect(userListPage.onUpdateVotes);
        NetworkController.showVotes.connect(userListPage.onShowVotes);

        // Populate with Users/Votes
        userListPage.onUpdateUsers();

        // Disable the clear button
        clearVotes.enabled = NetworkController.isAdmin();
    }
}
