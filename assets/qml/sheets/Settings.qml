import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Sheet {
    id: settingsSheet

    content: 
    Page {
        id: settingsPage
        
        // ==========================================================================================================
        // ==========================================================================================================
        
        titleBar: TitleBar {
            id: editDeckBar
            title: ""
            visibility: ChromeVisibility.Visible
            
            dismissAction: ActionItem {
                title: "Cancel"
                
                onTriggered: {
                    // Hide the Sheet
                    settingsSheet.close()
                }
            }
            
            acceptAction: ActionItem {
                title: "Save"
                
                onTriggered: {
                    // Check if valid
                    if (ipAddrBox.isValid) {
                        ProxySettings.setProxy(enableProxyToggle.checked);
                        ProxySettings.setCredentials(credentialsBox.user, credentialsBox.password);
                        ProxySettings.setHost(ipAddrBox.ip, ipAddrBox.port);

                        settingsSheet.close();
                    } else {
                        errToast.show();
                    }
                }                
            }           
            
        }        

        // ==========================================================================================================
        // ==========================================================================================================

		Container {
		    Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                topPadding: 10

                layout: DockLayout {
                }

                Label {
                    text: "Use Proxy Settings"
                    horizontalAlignment: HorizontalAlignment.Left
                    verticalAlignment: VerticalAlignment.Center

                    textStyle {
                        base: SystemDefaults.TextStyles.BodyText
                    }
                }

                ToggleButton {
                    id: enableProxyToggle
                    checked: false
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center                    
                    
                    onCheckedChanged: {
                        proxyInfo.visible = checked;
                    }
                }
            }

            Divider {
            }

            Container {
                horizontalAlignment: HorizontalAlignment.Fill

                Label {
                    id: infoBarTxt
                    text: "Changes will take effect next time a distributed session is joined"
                    textStyle.fontSize: FontSize.XXSmall
                    horizontalAlignment: HorizontalAlignment.Center
                    textFormat: TextFormat.Html
                    multiline: true
                    textStyle.textAlign: TextAlign.Center
                }
            }

            Divider {
            }
            
            Container {
                id: proxyInfo
                visible: enableProxyToggle.checked

                IPAddress {
                    id: ipAddrBox
                    label: "Host Address (SOCKS5) & Port:"
                    ip: "127.0.0.1"
                    port: "1080"
                }

                Divider {
                }

                LoginCredentials {
                    id: credentialsBox
                    label: "User Login:"
                }
            }           

        }

        // ==========================================================================================================
        // ==========================================================================================================
        
        onCreationCompleted: {
            // Load details from Settings
            enableProxyToggle.checked 	= ProxySettings.isProxyEnabled();
            credentialsBox.user 		= ProxySettings.getProxyUserName();
            credentialsBox.password 	= ProxySettings.getProxyPassword();
            ipAddrBox.ip 				= ProxySettings.getProxyHostIP();
            ipAddrBox.port 				= ProxySettings.getProxyHostPort();
        }

        attachedObjects: [
        	SystemToast {
                id: errToast
                body: "Host IP Address or Port are invalid. Cannot save."
            }
        ]
    }
}