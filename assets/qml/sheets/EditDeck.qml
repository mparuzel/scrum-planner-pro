import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Sheet {
    id: editDeckSheet

    content: 
    Page {
	    id: editDeckPage

        // ==========================================================================================================
        // ==========================================================================================================

        titleBar: TitleBar {
            id: editDeckBar
            title: ""
            visibility: ChromeVisibility.Visible

            dismissAction: ActionItem {
                title: "Cancel"
                onTriggered: {
                    // Hide the Sheet
                    editDeckSheet.close()
                }
            }

            acceptAction: ActionItem {
                title: "Save"
                onTriggered: {
                    //todo deckView.getDeck().size() > 0
                    // Save deck to repo
                    saveDeck();

					// Exit edit sheet
                    editDeckSheet.close();
                }
            }
        }

        // ==========================================================================================================
        // ==========================================================================================================

        Container {
	        horizontalAlignment: HorizontalAlignment.Fill
	        verticalAlignment: VerticalAlignment.Fill

	        Container {
	            Label {
	                text: "Name of Deck:"
	            }
	            TextArea {
	                id: deckNameTxt
	            	text: ""
	        	}
	        }
	
	        Divider {
	        }

	        Container {
	            horizontalAlignment: HorizontalAlignment.Fill
	            verticalAlignment: VerticalAlignment.Fill
	            
	            Label {
	                text: "Cards in this Deck:"
	            }
	
	            Container {
	                horizontalAlignment: HorizontalAlignment.Fill
	                verticalAlignment: VerticalAlignment.Fill
	                
	                DeckView {
	                    id: deckView
	
	                    onTriggered: {
	                        clearSelection();
	                        select(indexPath);
	
							// Enable delete button
	                        deleteBtn.enabled = isSelected(indexPath);
	                    }
	                }
	
	            }
	        }
	    }
	
	    // ==========================================================================================================
	    // ==========================================================================================================
	
	    actions: [
	        DeleteActionItem {
	            id: deleteBtn
	            enabled: false
	            
	            onTriggered: {
	                // Prompt and remove
	                removeCardDialog.show();
	            }
	            
	            attachedObjects: [
	                SystemDialog {
	                    id: removeCardDialog
	                    title: "Delete Card"
	                    body: "Are you sure you want to remove this card?"
	
	                    onFinished: {
	                        if (removeCardDialog.result == SystemUiResult.CancelButtonSelection) {
	                            return;
	                        }
	
							// Remove the card from the deck view
	                        deckView.remove(deckView.selectedItem);
	
							// Update enabled state
	                        deleteBtn.enabled = false;
	                    }
	                }
	            ]            
	        },
	        ActionItem {
	            id: addCard
	            title: qsTr("Add Card")
	            ActionBar.placement: ActionBarPlacement.OnBar
	            imageSource: "asset:///images/add_card.png"
	
	            onTriggered: {
	                addCardPrompt.show();
	            }
	            
	            attachedObjects: [
	                SystemPrompt {
	                    id: addCardPrompt
	                    title: "Add Card"
	                    body: "Enter the card's face value. HTML entities are allowed."
	
	                    onFinished: {
	                        // Ignore if Cancel button is pushed
	                        if (addCardPrompt.result == SystemUiResult.CancelButtonSelection) {
	                            return;
	                        }
	                        
	                        // Add user input to deck
	                        var field = addCardPrompt.inputFieldTextEntry();
                            deckView.add({text: field});
	                    }
	                }
	            ]
	        },
	        ActionItem {
	            id: addHalfCard
	            title: qsTr("Add Half Card")
	            ActionBar.placement: ActionBarPlacement.InOverflow
	            imageSource: "asset:///images/card_half.png"
	
	            onTriggered: {
	                deckView.add({text: "&#189;"});
	            }
	        },
	        ActionItem {
	            id: addInfinityCard
	            title: qsTr("Add Infinity Card")
	            ActionBar.placement: ActionBarPlacement.InOverflow
	            imageSource: "asset:///images/card_infinity.png"
	
	            onTriggered: {
	                deckView.add({text: "&#8734;"});
	            }
	        },
	        ActionItem {
	            id: addBreakCard
	            title: qsTr("Add Ellipsis Card")
	            ActionBar.placement: ActionBarPlacement.InOverflow
	            imageSource: "asset:///images/card_ellipsis.png"
	
	            onTriggered: {
	                deckView.add({text: "&#x2026;"});
	            }
	        }
	    ]
    
    }

    // ==========================================================================================================
    // ==========================================================================================================

    function editDeck(deckName, cardArr) {
        console.log("Editing deck: " + typeof (cardArr));

        editDeckBar.title = "Editing Deck";
        deckNameTxt.text = deckName;

        var cards = cardArr.toString().split(",");
        for (var i = 0; i < cards.length; i ++) {
            deckView.add({text: cards[i]});
        }
    }

    function newDeck() {
        editDeckBar.title = "Create New Deck";
        deckNameTxt.text = "New Deck";
    }

    function saveDeck() {
        var dataArr = deckView.getDeck();
        
        console.log("Saving deck: [" + deckNameTxt.text + "] with values: [" + dataArr + "]");
        DeckRepository.addDeck(deckNameTxt.text, deckView.getDeck(), true);
    }
}
