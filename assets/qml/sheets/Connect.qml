import bb.cascades 1.0

// Local Imports
import "../controls"

Sheet {
    id: connectingSheet
    
    content: 
    Page {
        Container {
            Header {
                title: qsTr("Scrum Planner Pro")
                subtitle: qsTr("Join Distributed Session")
            }

            Container {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                topPadding: 10.0

                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                ActivityIndicator {
                    id: spinner
                    running: true
                    preferredWidth: 100
                }

                Label {
                    id: titlestr
                    text: qsTr("Connecting...")
                    textStyle.fontWeight: FontWeight.Bold
                    textStyle.fontSize: FontSize.XXLarge
                    textStyle.textAlign: TextAlign.Center
                }
            }

            Divider {
            }

            Container {
                id: statusContainer
                visible: true

                Label {
                    text: "Status:"
                }

                Container {
                    leftPadding: 20.0
                    rightPadding: 20.0
                    clipContentToBounds: true
                    horizontalAlignment: HorizontalAlignment.Fill

                    Label {
                        id: statusStr
                        text: ""
                        textStyle.fontSize: FontSize.XXSmall
                        opacity: 0.5
                        multiline: true
                        // n-series: 8 lines
                        // l-series: 23 lines
                    }
                }
            }
        }

        actions: [
            ActionItem {
                id: backBtn
                title: "Cancel"
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/cancel.png"

                onTriggered: {
                    // Stop any connecting attempt
                    NetworkController.disconnect();

                    connectingSheet.close();
                }
            },
            ActionItem {
                id: enterSessionBtn
                title: "Join"
                enabled: false
                ActionBar.placement: ActionBarPlacement.OnBar
                imageSource: "asset:///images/forward.png"

                onTriggered: {
                    // Make the sheet disappear
                    connectingSheet.close();
                    
                    // Show net session page
                    var page = netSessionPage.createObject();
                    navigationPane.push(page);
                }

                attachedObjects: ComponentDefinition {
                    id: netSessionPage
                    source: "asset:///qml/pages/NetSession.qml"
                }
            }
        ]
    }

    // ==========================================================================================================
    // ==========================================================================================================

    function onUpdateStatus(msg) {
        statusStr.text += "> " + msg + "\n";
    }
    
    function onError(msg) {
        statusStr.text += "> " + msg + "\n";
        titlestr.text = "Error!";
        spinner.visible = false;
        backBtn.imageSource = "asset:///images/back.png";
        backBtn.title = "Back";
    }
    
    // Called when connecting is successful
    function onDisconnected(ok) {
        statusStr.text = "> Disconnected!\n";
        titlestr.text = "Disconnected";
        spinner.visible = false;
        enterSessionBtn.enabled = false;
        backBtn.imageSource = "asset:///images/back.png";
        backBtn.title = "Back";
    }
    
    function onConnected() {
        // Allow the user to enter into the session
        statusStr.text += "> Connected!\n> You may now enter into the planning session.\n";
        titlestr.text = "Connected";
        spinner.visible = false;
        enterSessionBtn.enabled = true;
        backBtn.imageSource = "asset:///images/back.png";
        backBtn.title = "Back";
    }
    
    function onConnecting() {
        titlestr.text = "Connecting...";
        statusStr.text = "";
        spinner.visible = true;
        backBtn.imageSource = "asset:///images/cancel.png";
        backBtn.title = "Cancel";
    }
    
    function startConnect(host, port, name) {
        // Start connecting
        NetworkController.connect(host, port, name);
    }

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        NetworkController.error.connect(onError);
        NetworkController.info.connect(onUpdateStatus);
        NetworkController.connected.connect(onConnected);
        NetworkController.connecting.connect(onConnecting);
        NetworkController.disconnected.connect(onDisconnected);
    }

}
