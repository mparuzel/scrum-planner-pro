import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

ListView {
    id: cardlist
    horizontalAlignment: HorizontalAlignment.Center
    implicitLayoutAnimationsEnabled: false

    dataModel: deckDataModel

    layout: GridListLayout {
        columnCount: 4
    }

    listItemComponents: [
	    ListItemComponent { 
	        type: "item"
	        
            content:
            CardItem {
                id: cardItem
                preferredHeight: 180
                preferredWidth: 180
                header: ListItemData.title
                text: ListItemData.text
                image: ListItemData.image
                border: ListItem.selected ? Color.create("#0098F0") : Color.DarkGray;
            }
        }
    ]

    attachedObjects: [
        GroupDataModel {
            id: deckDataModel
            grouping: ItemGrouping.None
            sortingKeys: ["meta"]
        }
    ]

    // ==========================================================================================================
    // ==========================================================================================================

    onTriggered: {
        clearSelection();        
        selectedItem = indexPath;

        // Emit signal
        selected(selectedItem);
    }    
    
    // ==========================================================================================================
    // ==========================================================================================================

	// Properties
	property variant selectedItem: null
	
	// Signals
	signal selected(variant item)

    // ==========================================================================================================
    // ==========================================================================================================

    function remove(idx) {
        if (idx != null) {
            deckDataModel.removeAt(idx);            
        }
        
        if (idx == selectedItem) {
            selectedItem = null;
        }
    }
    
    function removeAll() {
        while (deckDataModel.size() > 0) {
            deckDataModel.removeAt(deckDataModel.first());
        }
        
        selectedItem = null;
    }
    
    function updateAt(data) {
        var iter = deckDataModel.first();
        
        // Loop through and find the item in question
        for(var i = 0; i < deckDataModel.size(); i++) {
            var indx = deckDataModel.data(iter);
            
            // Do update
            if (indx["meta"] == data["meta"]) {
                if (data["title"] != null) indx["title"] = data["title"];
                if (data["text"] != null) indx["text"] = data["text"];
                if (data["image"] != null) indx["image"] = data["image"];
                
                // Update list
                deckDataModel.updateItem(iter, indx);
               
                return;
            }
            
            // Increase the iterator
            iter = deckDataModel.after(iter);
        }
        
        // If it gets here, just add
        add(data);
    }
    
    function add(cardDataMap) {
        var size = deckDataModel.size();
        var cardTitle = cardDataMap["title"];
        var cardStr = cardDataMap["text"];
        var cardImg = cardDataMap["image"];
        var cardMeta = cardDataMap["meta"];

        // Add to the deck list a card at the end
        deckDataModel.insert({title: cardTitle == null ? "" : cardTitle,
        					  text: cardStr == null ? "" : cardStr, 
        					  image: cardImg == null ? "" : cardImg, 
                              meta: cardMeta == null ? size : cardMeta});
    }
    
    function get(indexPath) {
        return deckDataModel.data(indexPath);
    }
    
    function getDeck() {
        var iter = deckDataModel.first();
        var arr = [];
        
        // Return an array of face values
        for( var i = 0; i < deckDataModel.size(); i++) {
            arr.push(deckDataModel.data(iter)["text"]);
            
            // Increase the iterator
            iter = deckDataModel.after(iter);
        }

        return arr;
    }

    function loadDeck() {
        var data = DeckRepository.activeDeckCards;

        deckDataModel.clear();
        
        // Populate the list view with values
        for (var i = 0; i < data.length; ++ i) {
            deckDataModel.insert({title: "",
                    			  text: data[i], 
            					  image: "", 
                                  meta: i});
        }
        
        selectedItem = null;
    }
}
