import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    Label {
        id: loginTitleTxt
        text: "Login Credentials:"
    }

    Container {
        layoutProperties: FlowListLayoutProperties {
        }

        layout: StackLayout {
            orientation: LayoutOrientation.RightToLeft
        }

        TextField {
            id: userIDTxt
            textStyle.fontSize: FontSize.Large
            input.submitKey: SubmitKey.None
        }
    }

    Label {
        text: "Password:"
    }

    Container {
        layoutProperties: FlowListLayoutProperties {
        }

        layout: StackLayout {
            orientation: LayoutOrientation.RightToLeft
        }

        TextField {
            id: passwordTxt
            textStyle.fontSize: FontSize.Large
            input.submitKey: SubmitKey.None
            inputMode: TextFieldInputMode.Password
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

    // Expose values of text boxes
    property alias user: userIDTxt.text
    property alias password: passwordTxt.text
    property alias label: loginTitleTxt.text

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
    }
}
