import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    horizontalAlignment: HorizontalAlignment.Fill

    DropDown {
        id: deckPicker
        title: "Selected Deck"
        selectedIndex: 0
        horizontalAlignment: HorizontalAlignment.Center
        translationY: 4.0

        onSelectedIndexChanged: {
            if (deckPicker.selectedOption != null) {
                selectDeckById(deckPicker.selectedOption.value);
            } else {
                selectDeckById(DeckRepository.availableDecks.length - 1);
            }
        }
    }

    Divider {
    }

    // ==========================================================================================================
    // ==========================================================================================================

	// Signals
	signal deckSelected();
	signal noDeckSelected();

	// Easy accessibility to Items
    property alias selectedItem: deckPicker.selectedOption
    
    // ==========================================================================================================
    // ==========================================================================================================

    function selectDeckById(id) 
    {
        var decks = DeckRepository.availableDecks;

        if (decks.length > 0 && id < decks.length) {
            // Set deck by name
            DeckRepository.activeDeck = decks[id];
            
            // Notify a deck was selected
            deckSelected(DeckRepository.activeDeck);
        } else {
            noDeckSelected();
        }
    }
    
    function removeAll()
    {
        // Remove all items (WARNING: Changes selected Id, causing active deck to be lost)
        deckPicker.removeAll();
    }
    
    function select(id)
    {
        // Loop through all items, matching name, then select
        for (var i = 0; i < deckPicker.count(); i++) {
            if (deckPicker.at(i).text == id) {
                deckPicker.selectedIndex = i;
                
                return;
            }
        }        
    }
    
    function populate(items)
    {
        var i = 0;
        
        // Populate the drop down
        while (items[i] != null) {
            var newOpt = newDropdownOption.createObject();
            newOpt.text = items[i];
            newOpt.value = i;

            deckPicker.insert(i, newOpt);

            i++;
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

    attachedObjects: [
        ComponentDefinition {
            id: newDropdownOption

            Option {
                // Dropdown list of Decks
            }
        }
    ]

}
