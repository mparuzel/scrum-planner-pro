import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    Label {
        id: labelTxt
        text: "Connect To (IP Address & Port):"
    }

    Container {
        layoutProperties: FlowListLayoutProperties {
        }

        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }

        TextField {
            id: ipAddrTxt
            inputMode: TextFieldInputMode.NumbersAndPunctuation
            input.submitKey: SubmitKey.None
            textStyle.fontSize: FontSize.Large
            text: "127.0.0.1"
            preferredWidth: 500

            validator: Validator {
                id: ipAddrValidator
                errorMessage: "Invalid IP Address"
                mode: ValidationMode.Immediate

                onValidate: {
                    // Regex check for IP Address
                    var regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
                    if (regex.test(ipAddrTxt.text)) {
                        state = ValidationState.Valid;
                    } else {
                        state = ValidationState.Invalid;
                    }

                    onValidateInput();
                }
            }
        }

        TextField {
            id: portNumberTxt
            inputMode: TextFieldInputMode.PhoneNumber
            textStyle.fontSize: FontSize.Large
            input.submitKey: SubmitKey.None
            text: "5335"

            validator: Validator {
                errorMessage: "Inavid Port Number"
                mode: ValidationMode.Immediate

                onValidate: {
                    // Regex check for IP Address
                    var regex = /^[0-9]{1,5}$/;
                    if (regex.test(portNumberTxt.text) && 
                        parseInt(portNumberTxt.text) < 65535 &&
                        parseInt(portNumberTxt.text) > 0) 
                    {
                        state = ValidationState.Valid;
                    } else {
                        state = ValidationState.Invalid;
                    }

                    onValidateInput();
                }
            }
        }
    }

    Label {
        id: ipAddrErr
        textStyle.textAlign: TextAlign.Right
        horizontalAlignment: HorizontalAlignment.Fill
        textStyle.fontStyle: FontStyle.Italic
        textStyle.fontWeight: FontWeight.Bold
        textStyle.color: Color.Red
        visible: false
    }

    // ==========================================================================================================
    // ==========================================================================================================

	// Expose values of text boxes 
	property alias ip: ipAddrTxt.text;
    property alias port: portNumberTxt.text;
    property alias label: labelTxt.text;
    
    // Expose validity state
    property variant isValid;

    // Signal notifying if state was changed
	signal updateValidation(bool isValid);
	
    // ==========================================================================================================
    // ==========================================================================================================

    function onValidateInput() {
        ipAddrErr.text = "";

        // Update state of error text
        if (ipAddrTxt.validator.state == ValidationState.Invalid) {
            ipAddrErr.text = ipAddrTxt.validator.errorMessage;
        } else if (portNumberTxt.validator.state == ValidationState.Invalid) {
            ipAddrErr.text = portNumberTxt.validator.errorMessage;
        }

        // If there is an error, display it
        ipAddrErr.visible = (ipAddrErr.text.length > 0);

        // Update state of field
        isValid = !(ipAddrErr.visible);

        updateValidation(isValid);
    }

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        onValidateInput();
    }
}