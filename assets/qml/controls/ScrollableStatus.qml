import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    preferredWidth: 720

    Label {
        id: titleTxt
        text: "Status:"
    }

    ScrollView {
        id: scroller

        Container {
            leftPadding: 20.0
            rightPadding: 20.0
            clipContentToBounds: true
            horizontalAlignment: HorizontalAlignment.Fill
            preferredWidth: 720

            layout: DockLayout {
            }

            Label {
                id: serverStatusStr
                text: ""
                textStyle.fontSize: FontSize.XXSmall
                opacity: 0.5
                multiline: true
            }
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

    // Expose values of text boxes
    property alias title: titleTxt.text
    property alias status: serverStatusStr.text

    // ==========================================================================================================
    // ==========================================================================================================

	function add(text) {
	    serverStatusStr.text += "> " + text + "\n";
	}
	
	function clear() {
	    serverStatusStr.text = "";
	}

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
    }
}