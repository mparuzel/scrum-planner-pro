import bb.cascades 1.0
import bb.system 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    Container {
        Label {
            text: qsTr("Your Name:")
        }

        Container {
            layoutProperties: FlowListLayoutProperties {
            }

            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            TextField {
                id: nameTxt
                inputMode: TextFieldInputMode.Text
                input.submitKey: SubmitKey.None
                text: "ScrumMaster"

                validator: Validator {
                    errorMessage: "Invalid User Name"
                    mode: ValidationMode.Immediate

                    onValidate: {
                        // Regex check for IP Address
                        var regex = /^[A-Za-z0-9]{1,}$/;
                        if (regex.test(nameTxt.text)) {
                            state = ValidationState.Valid;
                        } else {
                            state = ValidationState.Invalid;
                        }

                        onValidateInput();
                    }
                }
            }

            TextField {
                id: portNumberTxt
                inputMode: TextFieldInputMode.PhoneNumber
                textStyle.fontSize: FontSize.Large
                input.submitKey: SubmitKey.None
                visible: false
                text: "5335"

                validator: Validator {
                    errorMessage: "Invalid Port Number"
                    mode: ValidationMode.Immediate

                    onValidate: {
                        // Regex check for IP Address
                        var regex = /^[0-9]{2,5}$/;
                        if (regex.test(portNumberTxt.text) && parseInt(portNumberTxt.text) < 65535) {
                            state = ValidationState.Valid;
                        } else {
                            state = ValidationState.Invalid;
                        }

                        onValidateInput();
                    }
                }
            }
        }

        Label {
            id: nameErr
            textStyle.textAlign: TextAlign.Right
            horizontalAlignment: HorizontalAlignment.Fill
            textStyle.fontStyle: FontStyle.Italic
            textStyle.fontWeight: FontWeight.Bold
            textStyle.color: Color.Red
            visible: false
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

	// Aliases
	property alias name: nameTxt.text;
	property alias port: portNumberTxt.text;

    // Signal notifying if state was changed
    signal updateValidation(bool isValid);

    // ==========================================================================================================
    // ==========================================================================================================

	function addPortNumber() {
        portNumberTxt.visible = true;
        nameTxt.preferredWidth = 500;
    }

    function onValidateInput() {
        nameErr.text = "";

        // Update error state of name box
        if (nameTxt.validator.state == ValidationState.Invalid) {
            nameErr.text = nameTxt.validator.errorMessage;
        } else if (portNumberTxt.validator.state == ValidationState.Invalid) {
            nameErr.text = portNumberTxt.validator.errorMessage;
        }

        // If there is an error, display it
        nameErr.visible = (nameErr.text.length > 0);

        // Update state of field
        var isValid = !(nameErr.visible);

        updateValidation(isValid);
    }

    // ==========================================================================================================
    // ==========================================================================================================

	onCreationCompleted: {
        onValidateInput();
    }
}