import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

MenuDefinition {
    settingsAction: SettingsActionItem {
        onTriggered: {
            // Show settings
            var page = settingsPage.createObject();
            page.open();
        }

        attachedObjects: ComponentDefinition {
            id: settingsPage
            source: "asset:///qml/sheets/Settings.qml"
        }
    }
    /*
    helpAction: HelpActionItem {
        onTriggered: {
            //todo show hlep sheet
        }
    }*/
}