import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    id: card
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill

    layout: DockLayout {
    }

    Container {
        id: cardBacking
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        background: Color.DarkGray
        topPadding: 2.0
        bottomPadding: 2.0
        leftPadding: 2.0
        rightPadding: 2.0

        layout: DockLayout {
        }

        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            background: Color.create("#121212")

            layout: DockLayout {
            }

            CardFace {
                id: cardFace
            }
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

	// Aliases
    property alias text: cardFace.text
    property alias image: cardFace.image
    property alias header: cardFace.header
    property alias border: cardBacking.background

    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        cardFace.setThumb(true);
    }
	
}
