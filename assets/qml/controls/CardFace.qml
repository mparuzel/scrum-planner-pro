import bb.cascades 1.0

// Local Imports
import "../controls"
import "../sheets"
import "../pages"

Container {
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    layout: DockLayout {            
    }
    
    Container {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Top

        Label {
            id: topTxt
            text: ""
            //visible: (topTxt.text.length > 0 && cardImg.imageSource == "")
            textStyle.fontSize: FontSize.XXSmall
            horizontalAlignment: HorizontalAlignment.Center
        }
    }
    
    Container {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        Label {
            id: cardTxt
            text: "15"
            visible: (!cardImg.visible)
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 16
            textStyle.fontWeight: FontWeight.Bold
            textStyle.textAlign: TextAlign.Center
            textFormat: TextFormat.Html
            multiline: true
        }
        
        ImageView {
        	id: cardImg
        	visible: (cardImg.imageSource != "")
        }
    }

    // ==========================================================================================================
    // ==========================================================================================================

	// Aliases
    property alias header: topTxt.text
	property alias text: cardTxt.text
	property alias image: cardImg.imageSource

    // ==========================================================================================================
    // =========================aqw=================================================================================

    function setThumb(isThumb) {
        // Make the number smaller for thumbs view
        cardTxt.textStyle.fontSizeValue = isThumb ? 16 : 100;
    }
    
    // ==========================================================================================================
    // ==========================================================================================================

    onCreationCompleted: {
        setThumb(true);
    }
}
