/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Precompiled.h
 *
 *  Created on: May 1, 2013
 *      Author: mparuzel
 */
 
// Cascades Includes
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/ActionItem>
#include <bb/cascades/InvokeQuery>
#include <bb/cascades/Invocation>
#include <bb/cascades/Page>
#include <bb/cascades/Pickers/ContactPicker>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ArrayDataModel>
#include <bb/PpsObject>
#include <bb/pim/contacts/Contact>
#include <bb/pim/contacts/ContactService>
#include <bb/pim/account/AccountService>
#include <bb/pim/message/MessageService>
#include <bb/pim/message/ConversationBuilder>
#include <bb/pim/message/MessageBuilder>
#include <bb/pim/message/MessageContact>
#include <bb/platform/PlatformInfo>
#include <bb/platform/bbm/Context>
#include <bb/platform/bbm/RegistrationState>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeTargetReply>
#include <bb/system/InvokeQueryTargetsRequest>
#include <bb/system/InvokeQueryTargetsReply>
#include <bb/system/SystemDialog>
#include <bb/system/SystemProgressDialog>
#include <bb/system/SystemUiButton>
#include <bb/system/SystemUiProgressState>
#include <bb/system/SystemUiResult>
#include <bb/system/LocaleHandler>

// Qt Includes
#include <QMetaType>
#include <QDeclarativeEngine>
#include <QLocale>
#include <QTranslator>
#include <QUuid>
#include <QObject>
#include <QRegExp>
#include <QSettings>
#include <QByteArray>
#include <QVariantList>
#include <QVariantMap>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QHostAddress>
#include <QtXmlPatterns/QXmlSchema>
#include <QtXmlPatterns/QXmlSchemaValidator>

using namespace bb::cascades;
using namespace bb::cascades::pickers;
using namespace bb::pim::message;
using namespace bb::pim::account;
using namespace bb::pim::contacts;
using namespace bb::system;
