<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>Connect</name>
    <message>
        <location filename="../assets/qml/sheets/Connect.qml" line="13"/>
        <source>Scrum Planner Pro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/sheets/Connect.qml" line="14"/>
        <source>Join Distributed Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/sheets/Connect.qml" line="34"/>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditDeck</name>
    <message>
        <location filename="../assets/EditDeck.qml" line="12"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/qml/sheets/EditDeck.qml" line="128"/>
        <source>Add Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/sheets/EditDeck.qml" line="157"/>
        <source>Add Half Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/sheets/EditDeck.qml" line="167"/>
        <source>Add Infinity Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/sheets/EditDeck.qml" line="177"/>
        <source>Add Ellipsis Card</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Host</name>
    <message>
        <location filename="../assets/qml/pages/Host.qml" line="12"/>
        <source>Scrum Planner Pro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Host.qml" line="13"/>
        <source>Host Distributed Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Host.qml" line="39"/>
        <source>Inform your collaborators to connect to the &lt;b&gt;&lt;i&gt;IP Address&lt;/i&gt;&lt;/b&gt; listed above (wifi only). You can also inform them through Email or BBM using the &lt;b&gt;&lt;i&gt;Share&lt;/i&gt;&lt;/b&gt; button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Host.qml" line="220"/>
        <source>Start Hosting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Host.qml" line="234"/>
        <source>Enter Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Host.qml" line="252"/>
        <source>Share IP</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Local</name>
    <message>
        <location filename="../assets/qml/pages/Local.qml" line="130"/>
        <source>New Deck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Local.qml" line="148"/>
        <source>Edit Deck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Local.qml" line="170"/>
        <source>Delete Deck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Local.qml" line="203"/>
        <source>Share Deck</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../assets/qml/pages/Main.qml" line="27"/>
        <source>Scrum Planner Pro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Main.qml" line="28"/>
        <source>v1.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Main.qml" line="43"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/qml/pages/Main.qml" line="59"/>
        <source>Distributed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetSession</name>
    <message>
        <location filename="../assets/qml/pages/NetSession.qml" line="195"/>
        <source>View Users</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserName</name>
    <message>
        <location filename="../assets/qml/controls/UserName.qml" line="12"/>
        <source>Your Name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
