/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * App.h
 *
 *  Created on: Apr 19, 2013
 *      Author: mparuzel
 */

#ifndef PLANNINGPOKERAPP_H_
#define PLANNINGPOKERAPP_H_

// ===========================================================================

#include "Storage/DeckRepository.h"
#include "Storage/ServerRepository.h"
#include "Settings/ProxySettings.h"
#include "Settings/SessionSettings.h"
#include "Controllers/NetworkController.h"
#include "Invocation/InvokeManager.h"
#include "Network/Client.h"
#include "Network/Server.h"

// ===========================================================================

class PlanningPokerApp : public QObject
{
    Q_OBJECT

public:

    PlanningPokerApp( QObject *parent = NULL );
    virtual ~PlanningPokerApp();

    // Application Functionality
    void init( Application &app );

private:

    // Storage
    QDeckRepository         _deckRepo;
    QServerRepository       _svrRepo;

    // Controllers
    QNetworkController      _netCtl;

    // Managers
    QInvokeManager          _invoke;

    // Settings
    QSettings               _settings;
    QProxySettings          _proxy;
    QSessionSettings        _session;

};

// ===========================================================================

#endif
