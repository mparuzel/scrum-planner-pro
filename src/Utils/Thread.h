/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * thread.h
 *
 *  Created on: Apr 12, 2013
 *      Author: mparuzel
 */

#ifndef THREAD_H_
#define THREAD_H_

// ===========================================================================

class QThreadEx : public QThread
{
    Q_OBJECT

public:

    QThreadEx( const QString &name = "" );
    ~QThreadEx();

public slots:

    // Thread Functionality
    void start();
    void stop();
    bool isStarted() const;

signals:

    // Thread Signals
    void startThread();
    void stopThread();

private:

    // Thread entry point
    virtual void run();

private:

    // Thread Properties
    QString _threadName;

};

#endif
