/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Thread.cpp
 *
 *  Created on: Apr 12, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Utils/Thread.h"
#include "Utils/Log.h"

// ===========================================================================

QThreadEx::QThreadEx( const QString &name ) :
    _threadName( name )
{
    QThread::setTerminationEnabled( true );
}

QThreadEx::~QThreadEx()
{
    stop();
}

void QThreadEx::start()
{
    // If running, do nothing
    if ( QThread::isRunning() )
    {
        log( "Thread (%s) is already running.", qPrintable( _threadName ) );
        return;
    }

    log( "Starting thread (%s).", qPrintable( _threadName ) );

    // Tell the thread to initialize
    emit startThread();

    QThread::start();
}

void QThreadEx::stop()
{
    if ( QThread::isRunning() )
    {
        log( "Stopping thread (%s).", qPrintable( _threadName ) );

        // Tell thread client to quit processing
        emit stopThread();

        // Wait 3 seconds for processing to finish
        QThread::wait( 3000 );

        // If the thread is still active, terminate it forcefully
        if ( !QThread::isFinished() )
        {
            log( "Thread (%s) did not stop within the 2sec window. Terminating.", qPrintable( _threadName ) );

            QThread::terminate();
            QThread::wait();
        }
    }
}

void QThreadEx::run()
{
    log( "Thread (%s) is not implemented.", qPrintable( _threadName ) );
}

bool QThreadEx::isStarted() const
{
    return QThread::isRunning() && !QThread::isFinished();
}
