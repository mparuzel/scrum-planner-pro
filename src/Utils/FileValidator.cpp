/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * FileValidator.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Utils/FileValidator.h"
#include "Utils/Log.h"

// ===========================================================================

const QString g_DeckExt( ".ppdeck" );
const QString g_ServerInfoExt( ".ppsvr" );

const QString g_DeckDefn = "app/native/assets/xsd/DeckDefinition.xsd";
const QString g_ServerInfoDefn = "app/native/assets/xsd/ServerInfo.xsd";

// Global Instances
static QXmlSchema g_DeckSchema;
static QXmlSchema g_ServerInfoSchema;

// ===========================================================================

QFileValidator::QFileValidator()
{
}

QFileValidator::~QFileValidator()
{
}

bool QFileValidator::isValid( QFile &file ) const
{
    // Sanity check
    if ( !file.exists() )
    {
        log( "[QFileValidator] Bad file name." );
        return false;
    }

    log( "[QFileValidator] Validating file: %s", qPrintable( file.fileName() ) );

    // Do validation
    return validate( file.fileName(), file.readAll() );
}

bool QFileValidator::validate( const QString &fName, const QByteArray &data ) const
{
    QXmlSchema *schema = NULL;
    QString fileToCheck = QDir::currentPath() + "/";

    // Load the appropriate schema
    if ( fName.endsWith( g_DeckExt ) )
    {
        log( "[QFileValidator] Working with Deck file." );

        fileToCheck += g_DeckDefn;
        schema = &g_DeckSchema;
    }
    else if ( fName.endsWith( g_ServerInfoExt ) )
    {
        log( "[QFileValidator] Working with Server Info file." );

        fileToCheck += g_ServerInfoDefn;
        schema = &g_ServerInfoSchema;
    }
    else
    {
        log( "[QFileValidator] File (%s) does not have the correct extension.", qPrintable( fName ) );
        return false;
    }

    // Lazy initialization of the schema
    if ( !schema->isValid() &&
         !schema->load( fileToCheck ))
    {
        log( "[QFileValidator] Schema (%s) failed to load.", qPrintable( schema->documentUri().toString() ) );
        return false;
    }

    QXmlSchemaValidator validator( *schema );

    // Validate the XML against the XSD
    bool isValid = validator.validate( data );
    if ( !isValid )
    {
        log( "[QFileValidator] Validation failed." );
    }

    return isValid;
}


