/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * ConversionUtils.h
 *
 *  Created on: May 12, 2013
 *      Author: mparuzel
 */

#ifndef CONVERSIONUTILS_H_
#define CONVERSIONUTILS_H_

template<class T>
QVariantMap toVariantMap( const QMap<QString, T> &map )
{
    QVariantMap out;

    // Convert into a variant map
    foreach( const QString &key, map.keys() )
    {
        out[key] = QVariant( map[key] );
    }

    return out;
}

template<class T>
QVariantList toVariantList( const QList<T> &list )
{
    QVariantList varList;

    // Convert to Variant List
    foreach( const T &card, list )
    {
        varList.push_back( QVariant( card ) );
    }

    return varList;
}

#endif
