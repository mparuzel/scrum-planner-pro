/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * NetworkController.cpp
 *
 *  Created on: Apr 25, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Controllers/NetworkController.h"
#include "Messaging/Packet/PacketDefinitions.h"
#include "Storage/Deck.h"
#include "Utils/ConversionUtils.h"
#include "Utils/Log.h"

// ===========================================================================

// Constants
const QRegExp g_ClassA( "^10\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$" );
const QRegExp g_ClassB( "^172\.(1[6-9]|2[0-9]|3[0-1])\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$");
const QRegExp g_ClassC( "^192\.168\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$" );

// ===========================================================================

QNetworkController::QNetworkController( QDeckRepository &deckRepo ) :
    _deckRepo( deckRepo )
{
    // Hook up Server Signals for handling
    QObject::connect( &_server, SIGNAL( serverStarted() ), this, SLOT( onServerStarted() ) );
    QObject::connect( &_server, SIGNAL( serverStopped() ), this, SLOT( onServerStopped() ) );
    QObject::connect( &_server, SIGNAL( clientConnect( const QHostAddress & ) ), this, SLOT( onClientConnect( const QHostAddress & ) ) );
    QObject::connect( &_server, SIGNAL( clientDisconnect( const QHostAddress & ) ), this, SLOT( onClientDisconnect( const QHostAddress & ) ) );
    QObject::connect( &_server, SIGNAL( incomingData( const QHostAddress &, const QByteArray & ) ), &_packetHandler, SLOT( onDataForServer( const QHostAddress &, const QByteArray & ) ) );

    // Hook up Client Signals for Handling
    QObject::connect( &_client, SIGNAL( connected() ), this, SLOT( onConnected() ) );
    QObject::connect( &_client, SIGNAL( disconnected() ), this, SLOT( onDisconnected() ) );
    QObject::connect( &_client, SIGNAL( incomingData( const QByteArray & ) ), &_packetHandler, SLOT( onDataForClient( const QByteArray & ) ) );

    // Hook up Message Signals for handling
    QObject::connect( &_packetHandler, SIGNAL( greet( const QHostAddress & ) ), this, SLOT( onGreet( const QHostAddress & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( allUsers( const QClientList & ) ), this, SLOT( onAllUsers( const QClientList & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( bootUser( const QHostAddress & ) ), this, SLOT( onBootUser( const QHostAddress & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( switchDeck( const QDeck & ) ), this, SLOT( onSwitchDeck( const QDeck & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( allVotes( const QClientVotes & ) ), this, SLOT( onAllVotes( const QClientVotes & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( clearVotes() ), this, SLOT( onClearVotes() ) );
    QObject::connect( &_packetHandler, SIGNAL( userEntered( const QHostAddress &, const QString & ) ), this, SLOT( onUserEntered( const QHostAddress &, const QString & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( userLeft( const QHostAddress & ) ), this, SLOT( onUserLeft( const QHostAddress & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( userVotes( const QHostAddress &, const QString &, const QString & ) ), this, SLOT( onUserVotes( const QHostAddress &, const QString &, const QString & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( userConnected( const QHostAddress &, const QString & ) ), this, SLOT( onUserConnected( const QHostAddress &, const QString & ) ) );
    QObject::connect( &_packetHandler, SIGNAL( relayPacket( const QHostAddress &, const QByteArray & ) ), this, SLOT( onRelayPacket( const QHostAddress &, const QByteArray & ) ) );

    // Forward Server Signals
    QObject::connect( &_server, SIGNAL( serverStarted() ), this, SIGNAL( serverStarted() ) );
    QObject::connect( &_server, SIGNAL( serverStopped() ), this, SIGNAL( serverStopped() ) );
    QObject::connect( &_server, SIGNAL( error( const QString & ) ), this, SIGNAL( error( const QString & ) ) );
    QObject::connect( &_server, SIGNAL( info( const QString & ) ), this, SIGNAL( info( const QString & ) ) );

    // Forward Client Signals
    QObject::connect( &_client, SIGNAL( connecting() ), this, SIGNAL( connecting() ) );
    QObject::connect( &_client, SIGNAL( connected() ), this, SIGNAL( connected() ) );
    QObject::connect( &_client, SIGNAL( disconnected() ), this, SIGNAL( disconnected() ) );
    QObject::connect( &_client, SIGNAL( error( const QString & ) ), this, SIGNAL( error( const QString & ) ) );
    QObject::connect( &_client, SIGNAL( info( const QString & ) ), this, SIGNAL( info( const QString & ) ) );
}

QNetworkController::~QNetworkController()
{
    _client.disconnect();
    _server.stop();
}

// ===========================================================================
// ==                              ACCESSORS                                ==
// ===========================================================================

QVariant QNetworkController::getBestIP() const
{
    QList<QHostAddress> serverIPs;

    // Retrieve device interfaces
    const QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();

    foreach ( const QNetworkInterface &ifc, interfaces )
    {
        // Ignore Loopbacks and local addressing
        if ( ifc.flags() & QNetworkInterface::IsLoopBack ) continue;
        if ( ifc.flags() & QNetworkInterface::IsPointToPoint ) continue;

        if ( ifc.flags() & QNetworkInterface::IsUp )
        {
            const QList<QNetworkAddressEntry> entries = ifc.addressEntries();

            // Accumulate IP Addresses
            foreach ( const QNetworkAddressEntry &e, entries )
            {
                const QHostAddress addr = e.ip();
                const QString ipStr = addr.toString();

                log( "[QNetworkController] Found IP: %s", qPrintable( ipStr ) );

                // We only accept Calls A, B, and C ip addresses
                if ( !g_ClassA.exactMatch( ipStr ) &&
                     !g_ClassB.exactMatch( ipStr ) &&
                     !g_ClassC.exactMatch( ipStr ) ) continue;

                log( "[QNetworkController] Keeping IP: %s", qPrintable( ipStr ) );

                serverIPs.append( addr );
            }
        }
    }

    return serverIPs.isEmpty() ? QVariant() : QVariant( serverIPs[0].toString() );
}

QVariantMap QNetworkController::getClients() const
{
    return toVariantMap( _clients );
}

QVariantMap QNetworkController::getVotes() const
{
    return toVariantMap( _votes );
}

QVariant QNetworkController::getName() const
{
    return _name;
}

QVariant QNetworkController::getIP() const
{
    return _ip.toString();
}

QVariant QNetworkController::isAdmin() const
{
    return _server.isHosting();
}

// ===========================================================================
// ==                         CLIENT FUNCTIONALITY                          ==
// ===========================================================================

void QNetworkController::connect( const QString &ip, const QString &port, const QString &name )
{
    // Convert string to address
    if ( !_ip.setAddress( ip ) )
    {
        emit error( "Failed to convert IP Address [" + ip + ":" + port + "]." );
        emit disconnected();
    }

    // Save name entered into connect page
    _name = name;

    // Connect to Host
    if ( !_client.connect( _ip, port.toUShort() ) )
    {
        log( "[QNetworkController] Failed to connect." );
    }

    log( "[QNetworkController] Connecting to [%s:%s].", qPrintable( ip ), qPrintable( port ) );
}

void QNetworkController::disconnect()
{
    log( "[QNetworkController] Disconnecting." );

    // Disconnect from attempt or host
    _client.disconnect();

    // Reset state
    _ip.clear();
    _name.clear();
}

void QNetworkController::vote( const QString &card )
{
    // Sanity check
    if ( _client.isConnected() )
    {
        log( "[QNetworkController] Voting with card (%s).", qPrintable( card ) );

        QDeck deck = _deckRepo.getActiveDeck();
        if ( deck.isValidCard( card ) )
        {
            // Compose vote message
            QByteArray data = _packetHandler.createCastVotePacket( _ip, deck.getName(), card );
            if ( data.isEmpty() )
            {
                emit error( "Cannot cast vote. Data packet could not be constructed." );
                return;
            }

            // Send data
            _client.send( data );

            // Handle own vote
            onUserVotes( _ip, deck.getName(), card );
        }
        else
        {
            log( "[QNetworkController] Cannot Vote, card (%s) is not in active deck.", qPrintable( card ) );

            emit error( "Active deck does not contain selected Card. Please wait and try again (Sync error)." );
        }
    }
}

void QNetworkController::boot( const QString &ip )
{
    // Check if admin
    if ( _client.isConnected() &&
         _server.isHosting() )
    {
        QHostAddress to( ip );

        // Compose vote message
        QByteArray data = _packetHandler.createBootPacket( to );
        if ( data.isEmpty() )
        {
            emit error( "Cannot boot user. Data packet could not be constructed." );
            return;
        }

        // Send data
        _client.send( data );
    }
}

void QNetworkController::useDeck( const QString &deckName )
{
    // Check if admin
    if ( _client.isConnected() &&
         _server.isHosting() )
    {
        // Grab the deck, if it exists
        QDeck deck = _deckRepo.getDeck( deckName );
        if ( deck.isValid() )
        {
            // Compose vote message
            QByteArray data = _packetHandler.createSwitchDeckPacket( deck );
            if ( data.isEmpty() )
            {
                emit error( "Cannot switch deck. Data packet could not be constructed." );
                return;
            }

            // Send data
            _client.send( data );

            // Use the deck we selected and clear votes
            _deckRepo.setActiveDeck( deckName );
            _votes.clear();

            emit updateVotes();

            return;
        }

        emit error( "Chosen deck cannot be used because it's invalid." );
    }
}

void QNetworkController::clearVotes()
{
    // Check if admin
    if ( _client.isConnected() &&
         _server.isHosting() )
    {
        // Compose vote message
        QByteArray data = _packetHandler.createClearVotesPacket();
        if ( data.isEmpty() )
        {
            emit error( "Cannot clear votes. Data packet could not be constructed." );
            return;
        }

        // Send data
        _client.send( data );

        // Clear own votes
        _votes.clear();

        // Update UI
        emit updateVotes();
    }
}

// ===========================================================================
// ==                         SERVER FUNCTIONALITY                          ==
// ===========================================================================

void QNetworkController::startHost( const QString &portStr )
{
    log( "[QNetworkController] Starting server." );

    // Open the port and start the server
    _server.start( portStr.toUShort() );
}

void QNetworkController::stopHost()
{
    log( "[QNetworkController] Stopping server." );

    // Clear all connections and stop the server
    _server.stop();
}

// ===========================================================================
// ==                          PUBLIC CLIENT SLOTS                          ==
// ===========================================================================

void QNetworkController::onConnected()
{
    // Construct the user message
    QByteArray data = _packetHandler.createUserPacket( _name );
    if ( data.isEmpty() )
    {
        emit error( "Could not enter into planning session. Data error." );
        return;
    }

    // Send user info to server
    _client.send( data );
}

void QNetworkController::onDisconnected()
{
    // Reset state
    _ip.clear();
    _name.clear();
}

// ===========================================================================
// ==                          PUBLIC SERVER SLOTS                          ==
// ===========================================================================

void QNetworkController::onRelayPacket( const QHostAddress &who, const QByteArray &buf )
{
    // Unhandled server data gets relayed to everyone
    sendToAllButOne( who, buf );
}

void QNetworkController::onClientConnect( const QHostAddress &who )
{
    // Greet the user by sending back their IP to them
    sendToOne( who, _packetHandler.createGreetPacket( who ) );
}

void QNetworkController::onClientDisconnect( const QHostAddress &who )
{
    // Remove user form all lists
    _clients.remove( who.toString() );
    _votes.remove( who.toString() );

    // Construct user left message
    sendToAllButOne( who, _packetHandler.createUserLeftPacket( who ) );
}

void QNetworkController::onServerStopped()
{
    // Reset state
    _clients.clear();
    _votes.clear();
}

void QNetworkController::onServerStarted()
{
    // Reset state
    _clients.clear();
    _votes.clear();
}

// ===========================================================================
// ==                        MESSAGE HANDLING SLOTS                         ==
// ===========================================================================

void QNetworkController::onGreet( const QHostAddress &myIP )
{
    // Set data from server
    _ip = myIP.toString();
    _clients[myIP.toString()] = _name;

    emit updateSelf();
}

void QNetworkController::onAllUsers( const QClientList &clients )
{
    if ( !_server.isHosting() )
    {
        // Dumpt the old list and replace it with the new one
        _clients = clients;
    }

    emit updateUsers();
}

void QNetworkController::onSwitchDeck( const QDeck &deck )
{
    if ( deck.isValid() )
    {
        QVariantList vCards = toVariantList( deck.getCards() );

        // If deck names collide, resolve and overwrite
        _deckRepo.addDeck( deck.getName(), vCards, true );
        _deckRepo.setActiveDeck( deck.getName() );

        emit updateDeck();
    }

    // Dump votes
    _votes.clear();

    emit updateVotes();
}

void QNetworkController::onAllVotes( const QClientVotes &votes )
{
    // Copy over the votes of the session
    _votes = votes;

    emit updateVotes();
}

void QNetworkController::onClearVotes()
{
    // Clear votes
    _votes.clear();

    emit updateVotes();
}

void QNetworkController::onUserEntered( const QHostAddress &ip, const QString &name )
{
    // Add user to list
    _clients[ip.toString()] = name;

    emit userJoined( ip.toString(), name );
    emit updateVotes();
    emit updateUsers();
}

void QNetworkController::onUserLeft( const QHostAddress &who )
{
    QString client = _clients[who.toString()]; // Will create if doesn't exist

    // Remove user from list
    _clients.remove( who.toString() );
    _votes.remove( who.toString() );

    emit userLeft( who.toString(), client );
    emit updateVotes();
    emit updateUsers();

    // If the only non-voting user left, show all votes
    if ( _votes.size() == _clients.size() )
    {
        emit showVotes();
    }
}

void QNetworkController::onUserVotes( const QHostAddress &who, const QString &deck, const QString &card )
{
    QDeck curDeck = _deckRepo.getActiveDeck();

    // Check if vote is from this deck (lag can cause this)
    if ( !curDeck.isValidCard( card ) &&
         curDeck.getName() != deck )
    {
        emit error( _clients[who.toString()] + " is out of sync. Vote discarded (" + _clients[who.toString()] + " must revote)." );

        log( "[QNetworkController] Card (%s) or Deck (%s) is not in deck. Ignoring vote from (%s).", qPrintable( card ), qPrintable( deck ), qPrintable( who.toString() ) );
        return;
    }

    // User has voted
    _votes[who.toString()] = card;

    emit updateVotes();

    // If the only non-voting user left, show all votes
    if ( _votes.size() == _clients.size() )
    {
        emit showVotes();
    }
}

void QNetworkController::onBootUser( const QHostAddress &who )
{
    if ( who != _ip )
    {
        // Remove from the session, should trigger client disconnect
        _clients.remove( who.toString() );
        _server.kick( who );
    }
}

void QNetworkController::onUserConnected( const QHostAddress &who, const QString &name )
{
    // Add client to list of clients
    _clients[who.toString()] = name;

    // Inform of newly joined user of the state of the session
    sendToOne( who, _packetHandler.createSwitchDeckPacket( _deckRepo.getActiveDeck() ) );
    sendToOne( who, _packetHandler.createAllUsersPacket( _clients ) );
    sendToOne( who, _packetHandler.createAllVotesPacket( _votes ) );

    // Notify others that a new client has joined
    sendToAllButOne( who, _packetHandler.createUserEnteredPacket( who, name ) );
}

// ===========================================================================
// ==                            PRIVATE HELPERS                            ==
// ===========================================================================

void QNetworkController::sendToOne( const QHostAddress &who, const QByteArray &data )
{
    if ( !data.isEmpty() )
    {
        // Send a list of users to the client
        _server.send( who, data );
        return;
    }

    log( "[QNetworkController] Cannot send data to [%s] because buffer is empty.", qPrintable( who.toString() ) );
}

void QNetworkController::sendToAll( const QByteArray &data )
{
    // Send data to all
    sendToAllButOne( QHostAddress(), data );
}

void QNetworkController::sendToAllButOne( const QHostAddress &who, const QByteArray &data )
{
    // Sanity check
    if ( data.isEmpty() )
    {
        log( "[QNetworkController] Server cannot send data to clients because data is empty." );
        return;
    }

    foreach( const QString &ip, _clients.keys() )
    {
        QHostAddress to( ip );

        // Skip the person it was sent from
        if ( who.isNull() || to != who )
        {
            // Relay the message
            _server.send( to, data );
        }
    }
}
