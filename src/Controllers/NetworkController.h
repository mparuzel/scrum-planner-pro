/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * NetworkController.h
 *
 *  Created on: Apr 25, 2013
 *      Author: mparuzel
 */

#ifndef NETWORKCONTROLLER_H_
#define NETWORKCONTROLLER_H_

// ===========================================================================

#include "Messaging/PacketCommon.h"
#include "Messaging/PacketHandler.h"
#include "Storage/DeckRepository.h"
#include "Network/Client.h"
#include "Network/Server.h"
#include "Storage/Deck.h"

// ===========================================================================

class QNetworkController : public QObject
{
    Q_OBJECT

public:

    QNetworkController( QDeckRepository &deckRepo );
    virtual ~QNetworkController();

public slots:

    // Client Actions
    void connect( const QString &ip, const QString &port, const QString &name );
    void disconnect();
    void vote( const QString &card );
    void boot( const QString &ip );
    void useDeck( const QString &deckName );
    void clearVotes();

    // Server Actions
    void startHost( const QString &port );
    void stopHost();

    // Accessors
    QVariant    isAdmin() const;
    QVariant    getBestIP() const;
    QVariant    getName() const;
    QVariant    getIP() const;
    QVariantMap getClients() const;
    QVariantMap getVotes() const;

signals:

    // UI Signals
    void userJoined( const QString &ip, const QString &name );
    void userLeft( const QString &ip, const QString &name );
    void updateVotes();
    void updateUsers();
    void updateDeck();
    void updateSelf();
    void showVotes();

    // Signals Forwarded From QClient
    void connecting();
    void connected();
    void disconnected();

    // Signals Forwarded From QServer
    void serverStarted();
    void serverStopped();

    // General Signals
    void error( const QString &msg );
    void info( const QString &msg );

private slots:

    // Client Signal Handling (QClient)
    void onConnected();
    void onDisconnected();

    // Server Signal Handling (QServer)
    void onRelayPacket( const QHostAddress &who, const QByteArray &buf );
    void onClientConnect( const QHostAddress &who );
    void onClientDisconnect( const QHostAddress &who );
    void onServerStopped();
    void onServerStarted();

    // Signals Directed to Client (QPacketHandler)
    void onGreet( const QHostAddress &myIP );
    void onAllUsers( const QClientList &clients );
    void onSwitchDeck( const QDeck &deck );
    void onAllVotes( const QClientVotes &votes );
    void onClearVotes();
    void onUserEntered( const QHostAddress &ip, const QString &name );
    void onUserLeft( const QHostAddress &who );
    void onUserVotes( const QHostAddress &who, const QString &deck, const QString &card );

    // Signals Directed to Server (QPacketHandler)
    void onBootUser( const QHostAddress &who );
    void onUserConnected( const QHostAddress &who, const QString &name );

private:

    // Helpers
    void sendToOne( const QHostAddress &who, const QByteArray &data );
    void sendToAll( const QByteArray &data );
    void sendToAllButOne( const QHostAddress &who, const QByteArray &data );

private:

    // References to System
    QDeckRepository &   _deckRepo;

    // Network Stuff
    QClient             _client;
    QServer             _server;

    // Message Handling
    QPacketHandler      _packetHandler;

    // Session State (Client/Server Shared)
    QClientList         _clients;
    QClientVotes        _votes;

    // Personal State
    QHostAddress        _ip;
    QString             _name;
    QDeck               _deck;

    // todo mutex?

};

// ===========================================================================

#endif
