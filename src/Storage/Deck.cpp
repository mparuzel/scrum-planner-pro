/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Deck.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Storage/Deck.h"
#include "Utils/Log.h"

// ===========================================================================

const QString g_PlanningPokerTag = "PlanningPoker";
const QString g_DeckTag = "Deck";
const QString g_CardTag = "Card";

// ===========================================================================

QDeck::QDeck()
{
}

QDeck::QDeck( const QString &name ) :
    _name( name )
{
}

QDeck::QDeck( const QString &name, const QStringList &cards ) :
    _name( name ),
    _cards( cards )
{
}

QDeck::~QDeck()
{
}

bool QDeck::load( const QByteArray &xml )
{
    QXmlStreamReader reader( xml );

    while ( !reader.atEnd() )
    {
        reader.readNext();

        if ( reader.isStartElement() )
        {
            // Ignore the Scrum Planner Pro tag
            if ( reader.name() == g_PlanningPokerTag )
            {
                continue;
            }

            // Get the name from the Deck tag
            else if ( reader.name() == g_DeckTag )
            {
                QXmlStreamAttributes attr = reader.attributes();

                // Extract the name attribute
                _name = attr.value( "name" ).toString();
            }

            // Process card tag
            else if ( reader.name() == g_CardTag )
            {
                // Add card data
                _cards.push_back( reader.readElementText() );
            }
        }
    }

    // If an error has been encountered, clear state
    if ( reader.hasError() )
    {
        log( "[QDeck] Cannot parse file: %s.", qPrintable( reader.errorString() ) );

        _cards.empty();
        _name = "";
    }

    return !reader.hasError() && isValid();
}

void QDeck::addCard( const QString &val )
{
    _cards.push_back( val );
}

void QDeck::removeCard( const QString &val )
{
    _cards.removeOne( val );
}

bool QDeck::saveTo( QFile &file ) const
{
    // Check validity
    if ( !isValid() )
    {
        log( "[QDeck] Deck is not valid, cannot save." );
        return false;
    }

    // Open file
    if ( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        log( "[QDeck] Cannot open file for writing." );
        return false;
    }

    QXmlStreamWriter stream( &file );

    stream.setAutoFormatting( true );
    stream.writeStartDocument();

    stream.writeStartElement( g_PlanningPokerTag );     // <PlanningPoker>
    stream.writeStartElement( g_DeckTag );              //
    stream.writeAttribute( "name", _name );             //   <Deck name="foobar">

    // Write each card separately
    foreach ( const QString &card, _cards )
    {
        stream.writeStartElement( g_CardTag );          //     <Card>
        stream.writeCharacters( card );                 //       1/2
        stream.writeEndElement();                       //     </Card>
    }

    stream.writeEndElement();                           //   </Deck>
    stream.writeEndElement();                           // </PlanningPoker>

    stream.writeEndDocument();

    file.close();

    return true;
}

int QDeck::getNumCards() const
{
    return _cards.size();
}

QString QDeck::getName() const
{
    return _name;
}

QStringList QDeck::getCards() const
{
    return _cards;
}

void QDeck::clear()
{
    _name.clear();
    _cards.clear();
}

void QDeck::setName( const QString& name )
{
    _name = name;
}

void QDeck::setCards( const QStringList& cards )
{
    _cards = cards;
}

bool QDeck::isValidCard( const QString &card ) const
{
    return _cards.contains( card );
}

bool QDeck::isValid() const
{
    foreach ( const QString &card, _cards )
    {
        // Deck is invalid if a card has no value
        if ( card.isEmpty() ) return false;
    }

    return !_name.isEmpty() &&
           !_cards.isEmpty();
}

bool QDeck::operator == ( const QDeck &d ) const
{
    return _name == d._name &&
           _cards == d._cards;
}

bool QDeck::operator != ( const QDeck &d ) const
{
    return !( operator==( d ) );
}
