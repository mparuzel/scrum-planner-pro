/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * ServerRepository.cpp
 *
 *  Created on: May 4, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Storage/ServerRepository.h"
#include "Storage/ServerInfo.h"

// ===========================================================================

const QString g_DataDir     = "/data/";
const QString g_FileName    = "ServerInfo";
const QString g_ServerExt   = ".ppsvr";

// ===========================================================================

QServerRepository::QServerRepository()
{
}

QServerRepository::~QServerRepository()
{
}

QVariant QServerRepository::getServerFilePath( const QString &ip ) const
{
    QString filePath = QDir::currentPath() + g_DataDir + g_FileName + g_ServerExt;
    QFile file( filePath );
    QHostAddress addr( ip );
    QServerInfo svrInfo( addr );

    // Save server info to file before returning the path
    if ( svrInfo.isValid() &&
         svrInfo.saveTo( file ) )
    {
        return filePath;
    }

    return "";
}


