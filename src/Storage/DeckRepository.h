/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * DeckRepository.h
 *
 *  Created on: Apr 18, 2013
 *      Author: mparuzel
 */

#ifndef DECKREPOSITORY_H_
#define DECKREPOSITORY_H_

// ===========================================================================

class QDeck;

// ===========================================================================

class QDeckRepository : public QObject
{
    Q_OBJECT

    // Properties Accessible by QML
    Q_PROPERTY( QVariantList    availableDecks      READ getAvailableDecks )
    Q_PROPERTY( QVariantList    activeDeckCards     READ getActiveDeckCards )
    Q_PROPERTY( QVariant        activeDeck          READ getActiveDeckName      WRITE setActiveDeck )

public:

    QDeckRepository();
    ~QDeckRepository();

    // Accessors
    QDeck   getActiveDeck() const;
    QDeck   getDeck( const QString &name ) const;

signals:

    // Public Signals
    void deckListUpdated();
    void deckAlreadyExists( const QVariant &name );
    void deckDoesNotExist( const QVariant &name );
    void deckSelected( const QVariant &deckName, const QVariantList &cardList );
    void invalidDeck( const QVariant &name, const QVariantList &cardList );

public slots:

    // Accessors
    QVariantList getAvailableDecks() const;
    QVariantList getActiveDeckCards() const;
    QVariant     getActiveDeckName() const;
    QVariant     getActiveDeckShareFilePath() const;

    // Public Slots
    void setActiveDeck( const QVariant &name );
    void deleteDeck( const QVariant &name );
    void addDeck( const QVariant &name, const QVariantList &cards, bool overwrite = false );
    bool isDeck( const QVariant &name );

private:

    // Helper Funcitons
    void loadDecks();
    void loadDefaultDecks();
    bool addDeck( const QDeck &deck, bool overwrite = false );
    bool saveDeck( const QDeck &deck, bool overwrite = false );

private:

    typedef QMap<QString, QDeck> QDeckMap;

    QDeckMap    _decks;
    QString     _activeDeck;

};


#endif /* DECKREPOSITORY_H_ */
