/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * DeckRepository.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Storage/DeckRepository.h"
#include "Storage/Deck.h"
#include "Utils/FileValidator.h"
#include "Utils/ConversionUtils.h"
#include "Utils/Log.h"

// ===========================================================================

const QString g_DataDir     = "/data/";
const QString g_DecksDir    = "/data/decks/";
const QString g_DeckExt     = ".ppdeck";
const QString g_DeckShare   = "SharedDeck";
const QString g_FirstFile   = "First";

const QString g_DefaultDecks[] = { "Exponential Scale",
                                   "Cohn Scale",
                                   "Fibonacci",
                                   "T-Shirt"/*,
                                   "Rediculous Math"*/ };

const QStringList g_DefaultCards[] = { QString( "? 0 &#189; 1 2 4 8 16 32 64 128 256 &#8734; &#x2026;" ).split( " " ),
                                       QString( "? 0 &#189; 1 2 3 5 8 13 20 40 100 &#8734; &#x2026;" ).split( " " ),
                                       QString( "? 0 1 2 3 5 8 13 21 34 55 89 &#8734; &#x2026;" ).split( " " ),
                                       QString( "? XS S M L XL XXL &#8734; &#x2026;" ).split( " " )/*,
                                       QString( "? &#x221A;-1 0 <i>&#x03B2;</i> 1 <i>&#x03BB;</i> &#x03B6;(3) &#x221A;2 <i>&#x03C6;</i> &#x221A;3 <i>P</i>&#x2082; <i>K</i> <i>e</i> <i>&#x03C0;</i> <i>&#x03A8;</i> &#8734; &#x2026;" ).split( " " )*/ };

// ===========================================================================

QDeckRepository::QDeckRepository()
{
    // Load Decks from Files
    loadDecks();
    loadDefaultDecks();

    if ( !_decks.isEmpty() )
    {
        // Select the first deck
        setActiveDeck( _decks.keys().value( 0 ) );
    }

    // Notify UI that new decks are ready
    emit deckListUpdated();
}

QDeckRepository::~QDeckRepository()
{
}

// ===========================================================================
// ==                            DECK PROPERTIES                            ==
// ===========================================================================

QVariantList QDeckRepository::getAvailableDecks() const
{
    return toVariantList( _decks.keys() );
}

QVariant QDeckRepository::getActiveDeckName() const
{
    return _activeDeck;
}

QVariantList QDeckRepository::getActiveDeckCards() const
{
    return toVariantList( _decks[_activeDeck].getCards() );
}

QVariant QDeckRepository::getActiveDeckShareFilePath() const
{
    QString filePath = QDir::currentPath() + g_DecksDir + g_DeckShare + g_DeckExt;
    QFile file( filePath );

    // Save deck to file before returning the path
    if ( _decks[_activeDeck].saveTo( file ) )
    {
        return filePath;
    }

    return "";
}

// ===========================================================================
// ==                          DECK FUNCTIONALITY                           ==
// ===========================================================================

bool QDeckRepository::addDeck( const QDeck &deck, bool overwrite )
{
    if ( !deck.isValid() )
    {
        log( "[QDeckRepo] Deck is invalid, cannot add." );
        return false;
    }

    // Check if deck already exists, and fire a signal if it does
    if ( isDeck( deck.getName() ) && !overwrite )
    {
        log( "[QDeckRepo] Deck (%s) already exists.", qPrintable( deck.getName() ) );

        emit deckAlreadyExists( deck.getName() );
        return false;
    }

    // Add this deck to our list of decks
    saveDeck( deck, overwrite );

    _decks[deck.getName()] = deck;

    return true;
}

void QDeckRepository::loadDefaultDecks()
{
    QFile firstTime( QDir::currentPath() + g_DataDir + g_FirstFile );

    // If this is the first time that the app is running, create defaults
    if ( !firstTime.exists() )
    {
        firstTime.open( QFile::WriteOnly );

        for ( quint32 i = 0; i < sizeof( g_DefaultDecks ) / sizeof( g_DefaultDecks[0] ); ++i )
        {
            // Create a new deck and save it to deck directory
            addDeck( QDeck( g_DefaultDecks[i], g_DefaultCards[i] ) );
        }
    }
}

void QDeckRepository::loadDecks()
{
    QFileValidator fValid;
    QDir deckDir( QDir::currentPath() + g_DecksDir );

    if ( !deckDir.exists() )
    {
        // If the deck directory doesn't exist, make one
        deckDir.mkpath( deckDir.path() );
    }

    // Get all files in the deck Directory
    QStringList files = deckDir.entryList( QDir::Files | QDir::NoDotAndDotDot, QDir::Name );
    foreach ( const QString &fName, files )
    {
        QString filePath = deckDir.path() + "/" + fName;

        log( "[QDeckRepo] Loading file %s.", qPrintable( filePath ) );

        QFile file( filePath );
        if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            // Check validity of the file against an XML schema
            if ( !fValid.isValid( file ) )
            {
                log( "[QDeckRepo] Deck (%s) failed validation.", qPrintable( fName ) );
                continue;
            }

            QDeck newDeck;

            // Reset file to the beginning for reading
            file.seek( 0 );

            // Load the deck from file data
            if ( newDeck.load( file.readAll() ) )
            {
                _decks[newDeck.getName()] = newDeck;
                continue;
            }

            log( "[QDeckRepo] Deck (%s) failed to be parsed.", qPrintable( fName ) );
        }
        else
        {
            log( "[QDeckRepo] Failed to open and read deck file (%s).", qPrintable( fName ) );
        }
    }
}

bool QDeckRepository::saveDeck( const QDeck &deck, bool overwrite )
{
    QString path = QDir::currentPath() + g_DecksDir + deck.getName() + g_DeckExt;
    QFile file( path );

    // If it exists already and overwrite is not specified, ignore it
    if ( file.exists() && !overwrite )
    {
        log( "[QDeckRepo] File [%s] already exists. Ignoring.", qPrintable( path ) );
        return false;
    }

    log( "[QDeckRepo] Saving file %s.", qPrintable( path ) );

    // Write deck contents to file
    if ( !deck.saveTo( file ) )
    {
        log( "[QDeckRepo] Failed to save deck to file." );
        return false;
    }

    return true;
}

QDeck QDeckRepository::getActiveDeck() const
{
    return _decks[_activeDeck];
}

QDeck QDeckRepository::getDeck( const QString &name ) const
{
    // Return invalid deck if name doesn't exist
    return _decks.contains( name ) ? _decks[name] : QDeck();
}

// ===========================================================================
// ==                             PUBLIC SLOTS                              ==
// ===========================================================================

void QDeckRepository::addDeck( const QVariant &name, const QVariantList &cards, bool overwrite )
{
    if ( name.toString().isEmpty() || cards.isEmpty() )
    {
        emit invalidDeck( name, cards );
    }
    else
    {
        QStringList strList;

        // Convert to QStringList
        foreach( const QVariant &card, cards )
        {
            strList.push_back( card.toString() );
        }

        // Attempt to add the deck
        addDeck( QDeck( name.toString(), strList ), overwrite );

        // Update the deck on the screen
        setActiveDeck( name.toString() );

        // Notify UI that new decks are ready
        emit deckListUpdated();
    }
}

void QDeckRepository::deleteDeck( const QVariant &name )
{
    // Sanity check
    if ( name.toString().isEmpty() )
    {
        emit deckDoesNotExist( name );
    }
    else
    {
        QString path = QDir::currentPath() + g_DecksDir + name.toString() + g_DeckExt;
        QFile file( path );

        if ( file.exists() )
        {
            // Delete file
            file.remove();
        }

        // Remove deck from list
        _decks.remove( name.toString() );

        // If the deck is current, switch to some other deck or null
        if ( _activeDeck == name.toString() )
        {
            _activeDeck = "";

            if ( !_decks.isEmpty() )
            {
                // Select the first available deck
                setActiveDeck( _decks.keys().value( 0 ) );
            }
        }

        emit deckListUpdated();
    }
}

void QDeckRepository::setActiveDeck( const QVariant &name )
{
    if ( !isDeck( name ) )
    {
        // This technically should not happen
        emit deckDoesNotExist( name );
    }
    else
    {
        _activeDeck = name.toString();

        // Notify listeners of deck change
        emit deckSelected( name, getActiveDeckCards() );
    }
}

bool QDeckRepository::isDeck( const QVariant &name )
{
    return _decks.contains( name.toString() );
}

