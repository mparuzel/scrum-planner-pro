/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * ServerInfo.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Storage/ServerInfo.h"
#include "Utils/Log.h"

// ===========================================================================

const QString g_PlanningPokerTag = "PlanningPoker";
const QString g_ServerInfoTag = "ServerInfo";

// ===========================================================================

QServerInfo::QServerInfo()
{
}

QServerInfo::QServerInfo( const QHostAddress &host ) :
    _host( host )
{
}

QServerInfo::~QServerInfo()
{
}

bool QServerInfo::load( const QByteArray &xml )
{
    QXmlStreamReader reader( xml );

    while ( !reader.atEnd() && !reader.hasError() )
    {
        reader.readNext();

        if ( reader.isStartElement() )
        {
            // Ignore the Scrum Planner Pro tag
            if ( reader.name() == g_PlanningPokerTag )
            {
                continue;
            }

            // Get the name from the Deck tag
            else if ( reader.name() == g_ServerInfoTag )
            {
                QXmlStreamAttributes attr = reader.attributes();
                QString hostIPStr = attr.value( "ip" ).toString();

                // Extract the name attribute
                if ( !_host.setAddress( hostIPStr ) )
                {
                    log( "[QServerInfo] Cannot set host IP address [%s].", qPrintable( hostIPStr ) );
                }
            }
        }
    }

    // If an error has been encountered, clear state
    if ( reader.hasError() )
    {
        log( "[QServerInfo] Cannot parse file: %s.", qPrintable( reader.errorString() ) );

        _host.clear();
    }

    return !reader.hasError() && isValid();
}

void QServerInfo::setHost( const QString &val )
{
    // Convert string to host IP
    if ( !_host.setAddress( val ) )
    {
        log( "[QServerInfo] Cannot convert string [%s] into host address.", qPrintable( val ) );
    }
}

bool QServerInfo::saveTo( QFile &file ) const
{
    // Check validity
    if ( !isValid() )
    {
        log( "[QServerInfo] Cannot save server info, not valid." );
        return false;
    }

    // Open file
    if ( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        log( "[QServerInfo] Cannot open file for writing." );
        return false;
    }

    QXmlStreamWriter stream( &file );

    stream.setAutoFormatting( true );
    stream.writeStartDocument();

    stream.writeStartElement( g_PlanningPokerTag );     // <PlanningPoker>
    stream.writeStartElement( g_ServerInfoTag );        //
    stream.writeAttribute( "ip", _host.toString() );    //   <ServerInfo ip="10.24.56.233">
    stream.writeEndElement();                           //   </ServerInfo>
    stream.writeEndElement();                           // </PlanningPoker>

    stream.writeEndDocument();

    file.close();

    return true;
}

QHostAddress QServerInfo::getHostIP() const
{
    return _host;
}

bool QServerInfo::isValid() const
{
    return !_host.isNull();
}


