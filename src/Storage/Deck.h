/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Deck.h
 *
 *  Created on: Apr 26, 2013
 *      Author: mparuzel
 */

#ifndef DECK_H_
#define DECK_H_

// ===========================================================================

class QDeck
{
public:

    QDeck();
    QDeck( const QString &name );
    QDeck( const QString &name, const QStringList &cards );
    virtual ~QDeck();

    // Deck Functionality
    bool load( const QByteArray &xml );
    bool saveTo( QFile &file ) const;
    void clear();

    // Card Functionality
    void addCard( const QString &val );
    void removeCard( const QString &val );

    // Mutators
    void setName( const QString &name );
    void setCards( const QStringList &cards );

    // Accessors
    int         getNumCards() const;
    QString     getName() const;
    QStringList getCards() const;
    bool        isValidCard( const QString &card ) const;
    bool        isValid() const;

    // Overloaded Operators
    bool operator==( const QDeck &d ) const;
    bool operator!=( const QDeck &d ) const;

private:

    QString     _name;
    QStringList _cards;
//todo maybe use a GUID in case of deck name collision during distributed session
};

// ===========================================================================

Q_DECLARE_METATYPE(QDeck)

// ===========================================================================

#endif
