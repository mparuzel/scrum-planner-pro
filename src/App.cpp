/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *y
 * App.cpp
 *
 *  Created on: Apr 19, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "App.h"
#include "Utils/Log.h"

// ===========================================================================

// Constants
const QString SETTINGS_FILEPATH = QDir::currentPath() + "/" + "data/Settings/Settings.ini";

// ===========================================================================

PlanningPokerApp::PlanningPokerApp( QObject *parent ) : QObject( parent ),
    _netCtl( _deckRepo ),
    _settings( SETTINGS_FILEPATH, QSettings::IniFormat ),
    _proxy( _settings ),
    _session( _settings )
{
}

PlanningPokerApp::~PlanningPokerApp()
{
}

void PlanningPokerApp::init( Application &app )
{
    // Create the entry point to the application UI
    QmlDocument *qml = QmlDocument::create("asset:///qml/pages/Main.qml").parent( this );

    // Allow access to these classes from QML
    qml->setContextProperty( QLatin1String( "InvokeManager" ), &_invoke );
    qml->setContextProperty( QLatin1String( "DeckRepository" ), &_deckRepo );
    qml->setContextProperty( QLatin1String( "ServerRepository" ), &_svrRepo );
    qml->setContextProperty( QLatin1String( "NetworkController" ), &_netCtl );
    qml->setContextProperty( QLatin1String( "ProxySettings" ), &_proxy );
    qml->setContextProperty( QLatin1String( "SessionSettings" ), &_session );

    // Create the root object of our scene
    app.setScene( qml->createRootObject<AbstractPane>() );
}
