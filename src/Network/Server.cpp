/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Server.cpp
 *
 *  Created on: Apr 12, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Network/Server.h"
#include "Network/SocketUtil.h"
#include "Network/Defines.h"
#include "Utils/Thread.h"
#include "Utils/Log.h"

// ===========================================================================

QServer::QServer() :
    _mutex( QMutex::Recursive )
{
    // Hook up server events
    connect( &_server, SIGNAL( newConnection() ), this, SLOT( onNewConnection() ) );
}

QServer::~QServer()
{
    stop();
}

// ===========================================================================
// ==                          SERVER FUNCTIONALITY                         ==
// ===========================================================================

void QServer::start( quint16 port )
{
    QMutexLocker lock( &_mutex );

    emit info( "Initializing Server on port: " + QString::number( port ) + "." );

    // Don't use a proxy when hosting (TODO?)
    _server.setProxy( QNetworkProxy::NoProxy );

    // Begin listening for connections
    if ( _server.listen( QHostAddress::Any, BIND_PORT ) )
    {
        _server.setMaxPendingConnections( LISTEN_BACKLOG );

        emit info( "Setting max connections to 32." );
        emit serverStarted();
    }
    else
    {
        emit error( "Cannot start hosting on port: " + QString::number( port ) + "." );
    }
}

void QServer::stop()
{
    QMutexLocker lock( &_mutex );

    if ( _server.isListening() )
    {
        // Terminate server
        _server.close();

        while ( !_clients.isEmpty() )
        {
            QTcpSocket *pSocket = _clients.begin().value();

            if ( pSocket->isOpen() )
            {
                // Close client socket, will call onSocketDisconnect() and remove the socket from the list
                pSocket->close();
                pSocket->waitForDisconnected( GIVE_UP_TIMEOUT_MS );

                if ( pSocket->state() != QAbstractSocket::UnconnectedState )
                {
                    // Force abort if disconnect is taking too long
                    pSocket->abort();
                }
            }
            else
            {
                log( "[QServer] Socket was already disconnected. Ignoring." );

                // Remove from list (TODO: can this cause a problem?)
                _clients.remove( _clients.begin().key() );
            }
        }

        emit info( "Server is shutting down." );
        emit serverStopped();
    }
}

void QServer::send( const QHostAddress &to, const QByteArray &buf )
{
    // Sanity
    if ( buf.isEmpty() )
    {
        log( "[QServer] Cannot send empty buffer to [%s].", qPrintable( to.toString() ) );
        return;
    }

    QMutexLocker lock( &_mutex );

    // Don't send data when server is closing
    if ( !_server.isListening() )
    {
        log( "[QServer] Data send ignored, server is shutting down." );
        return;
    }

    if ( _clients.contains( to.toString() ) )
    {
        log( "[QServer] Sending Data To (%s): [%s]", qPrintable( to.toString() ), buf.constData() );

        // Send data to remote device
        qint64 wrote = _clients[to.toString()]->write( buf + PACKET_SEP );
        if ( wrote != buf.size() )
        {
            log( "[QServer] Wrote only [%d] bytes to socket when buffer contains [%d] size.", wrote, buf.size() );
        }
    }
    else
    {
        log( "[QServer] IP (%s) doesn't exist. Data not sent.", qPrintable( to.toString() ) );
    }
}

void QServer::kick( const QHostAddress &who )
{
    QMutexLocker lock( &_mutex );

    if ( _clients.contains( who.toString() ) )
    {
        emit info( "Client (" + who.toString() + ") is being kicked." );

        // Close client socket, will invoke onSocketDisconnected()
        _clients[who.toString()]->close();
    }
}

bool QServer::isHosting() const
{
    QMutexLocker lock( const_cast<QMutex *>( &_mutex ) );

    return _server.isListening();
}

bool QServer::isEmpty() const
{
    QMutexLocker lock( const_cast<QMutex *>( &_mutex ) );

    return _clients.isEmpty();
}

// ===========================================================================
// ==                        SOCKET SIGNAL HANDLING                         ==
// ===========================================================================

void QServer::onSocketError( QAbstractSocket::SocketError errType )
{
    // Notify listener of error
    emit error( QSocketUtil::toString( errType ) );
}

void QServer::onNewConnection()
{
    QMutexLocker lock( &_mutex );

    // Grab the client socket
    QTcpSocket *pClient = _server.nextPendingConnection();
    if ( pClient )
    {
        QHostAddress addr = pClient->peerAddress();
        if ( !addr.isNull() )
        {
            _clients[addr.toString()] = pClient;

            log( "[QServer] New socket (%s) Connected.", qPrintable( addr.toString() ) );

            // Hook up socket to provide state change info to the server
            connect( pClient, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( onSocketError( QAbstractSocket::SocketError ) ) );
            connect( pClient, SIGNAL( disconnected() ), this, SLOT( onSocketDisconnect() ) );
            connect( pClient, SIGNAL( readyRead() ), this, SLOT( onSocketData() ) );

            // Unlock server to not cause a deadlock
            lock.unlock();

            // Notify listeners of new client
            emit clientConnect( addr );
        }
        else
        {
            pClient->abort();
            delete pClient;

            log( "[QServer] Cannot get socket's peer address." );
        }
    }
}

void QServer::onSocketDisconnect()
{
    QString who;

    // Grab the socket object
    QTcpSocket *socket = qobject_cast<QTcpSocket *>( sender() );
    if ( socket )
    {
        QMutexLocker lock( &_mutex );

        // Socket may not have an IP address anymore, so look up by file descriptor
        for ( QClients::iterator it = _clients.begin();
              it != _clients.end();
              ++it )
        {
            QTcpSocket *clientSock = it.value();

            if ( clientSock->socketDescriptor() == socket->socketDescriptor() )
            {
                who = it.key();

                log( "[QServer] Socket (%s) disconnected.", qPrintable( who ) );

                // Close the open client connection
                _clients.remove( who );

                break;
            }
        }

        // Exit if we're no longer listening
        if ( !isHosting() )
        {
            return;
        }
    }

    if ( !who.isEmpty() )
    {
        // Notify listeners of client disconnect
        emit clientDisconnect( QHostAddress( who ) );
    }
}

void QServer::onSocketData()
{
    // Grab the socket object
    QTcpSocket *socket = qobject_cast<QTcpSocket *>( sender() );
    if ( socket )
    {
        QHostAddress who = socket->peerAddress();
        QByteArray data = socket->readAll();

        if ( !data.isEmpty() )
        {
            // Data can be combined if the packets are small enough
            QList<QByteArray> packets = data.split( PACKET_SEP );

            foreach( const QByteArray &buf, packets )
            {
                if ( buf.isEmpty() ) continue;

                log( "[QServer] Recieving Data from (%s): [%s]", qPrintable( who.toString() ), buf.constData() );

                // Notify listener of new data from client
                emit incomingData( who, buf );
            }
        }
        else
        {
            log( "[QServer] Data is available from (%s), none was read.", qPrintable( who.toString() ) );
        }
    }
}
