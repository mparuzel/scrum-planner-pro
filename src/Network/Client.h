/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Client.h
 *
 *  Created on: Apr 12, 2013
 *      Author: mparuzel
 */

#ifndef CLIENT_H_
#define CLIENT_H_

// ===========================================================================

class QClient : public QObject
{
    Q_OBJECT

public:

    QClient();
    virtual ~QClient();

    // Socket Functionality
    bool connect( const QHostAddress &host, quint16 port );
    void disconnect();
    bool send( const QByteArray &buf );

    // Accessors
    bool isConnected() const;
    bool isConnecting() const;

signals:

    // Socket Signals
    void connecting();
    void connected();
    void disconnected();
    void incomingData( const QByteArray &data );
    void error( const QString &msg );
    void info( const QString &msg );

private slots:

    // Event Handlers for QTcpSocket
    void onStateChanged( QAbstractSocket::SocketState );
    void onIncomingData();
    void onDisconnect();
    void onConnect();
    void onError( QAbstractSocket::SocketError errType );

private:

    QMutex          _mutex;
    QTcpSocket      _sock;

};

// ===========================================================================

#endif
