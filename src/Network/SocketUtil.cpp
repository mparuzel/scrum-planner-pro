/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * SocketUtil.cpp
 *
 *  Created on: Apr 25, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Network/SocketUtil.h"

// ===========================================================================

const QString g_State[] = { "Not connected.",
                            "Looking up host...",
                            "Connecting to Server...",
                            "Connected to Server.",
                            "Attempting to bind on Port 5335.",
                            "Listening for connections...",
                            "Closing connection." };

const QString g_Error[] = { "The connection was refused by the peer (or timed out).",
                            "The remote host closed the connection.",
                            "The host address was not found.",
                            "The socket operation failed due to lack of required privileges.",
                            "The local system ran out of resources.",
                            "The socket operation timed out.",
                            "The datagram was larger than the operating system's limit.",
                            "An error occurred with the network. Check network connectivity.",
                            "TCP Port is already in use.",
                            "UDP port is already in use.",
                            "The requested socket operation is not supported.",
                            "The socket is using a proxy, and the proxy requires authentication.",
                            "The SSL/TLS handshake failed, closing connection.",
                            "The last operation attempted has not finished yet.",
                            "The server denied the proxy connection.",
                            "The connection to the proxy server was closed unexpectedly",
                            "The connection to the proxy server timed out.",
                            "The proxy address was not found.",
                            "The connection negotiation with the proxy server was not understood." };

// ===========================================================================

QString QSocketUtil::toString( QAbstractSocket::SocketState state )
{
    return g_State[state];
}

QString QSocketUtil::toString( QAbstractSocket::SocketError errType )
{
    return errType != QAbstractSocket::UnknownSocketError ? g_Error[errType] : "Internal socket error.";
}

