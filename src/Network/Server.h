/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Server.h
 *
 *  Created on: Apr 12, 2013
 *      Author: mparuzel
 */

#ifndef SERVER_H_
#define SERVER_H_

// ===========================================================================

#include "Utils/Thread.h"

// ===========================================================================

class QServer : public QObject
{
    Q_OBJECT

public:

    QServer();
    virtual ~QServer();

    // Server Functionality
    void start( quint16 port );
    void stop();
    void send( const QHostAddress &to, const QByteArray &buf );
    void kick( const QHostAddress &who );

    // Accessors
    bool isHosting() const;
    bool isEmpty() const;

signals:

    // Socket Signals
    void serverStarted();
    void serverStopped();
    void clientConnect( const QHostAddress &who );
    void clientDisconnect( const QHostAddress &who );
    void incomingData( const QHostAddress &who, const QByteArray &buf );
    void error( const QString &msg );
    void info( const QString &msg );

private slots:

    // Slots Called by QTcpServer
    void onNewConnection();

    // Slots Called by (Client) QTcpSocket
    void onSocketDisconnect();
    void onSocketData();
    void onSocketError( QAbstractSocket::SocketError errType );

private:

    typedef QMap<QString /* IP Address */, QTcpSocket *> QClients;

    QMutex      _mutex;
    QTcpServer  _server;
    QClients    _clients;

};

// ===========================================================================

#endif
