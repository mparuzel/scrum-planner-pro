/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Client.cpp
 *
 *  Created on: Apr 12, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Network/Client.h"
#include "Network/SocketUtil.h"
#include "Network/Defines.h"
#include "Utils/Log.h"

// ===========================================================================

QClient::QClient() :
    _mutex( QMutex::Recursive )
{
    // Hook up socket events
    QObject::connect( &_sock, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( onError( QAbstractSocket::SocketError ) ) );
    QObject::connect( &_sock, SIGNAL( stateChanged ( QAbstractSocket::SocketState ) ), this, SLOT( onStateChanged( QAbstractSocket::SocketState ) ) );
    QObject::connect( &_sock, SIGNAL( disconnected() ), this, SLOT( onDisconnect() ) );
    QObject::connect( &_sock, SIGNAL( connected() ), this, SLOT( onConnect() ) );
    QObject::connect( &_sock, SIGNAL( readyRead() ), this, SLOT( onIncomingData() ) );
}

QClient::~QClient()
{
    _sock.abort();
}

// ===========================================================================
// ==                         CLIENT FUNCTIONALITY                          ==
// ===========================================================================

bool QClient::connect( const QHostAddress &hostIP, quint16 port )
{
    QMutexLocker lock( &_mutex );

    // Start UI update
    emit connecting();

    QNetworkProxy curProxy = QNetworkProxy::applicationProxy();
    if ( curProxy.type() != QNetworkProxy::NoProxy )
    {
        emit info( "Using proxy: " + curProxy.hostName() );

        // Set connection to use a proxy
        _sock.setProxy( curProxy );
    }

    // Use the application level proxy
    _sock.setProxy( curProxy );

    // If the socket is connected, close it
    if ( _sock.isOpen() )
    {
        _sock.abort();
    }

    emit info( "Connecting to: " + hostIP.toString() + ":" + QString::number( port ) );

    // Begin connect
    _sock.connectToHost( hostIP, port );

    return true;
}

void QClient::disconnect()
{
    QMutexLocker lock( &_mutex );

    emit info( "Disconnecting..." );

    // Disconnect gracefully if connected
    _sock.disconnectFromHost();
    _sock.waitForDisconnected( GIVE_UP_TIMEOUT_MS );

    if ( _sock.state() != QAbstractSocket::UnconnectedState )
    {
        // Force abort if disconnect is taking too long
        _sock.abort();
    }

    // Notify of disconnected state
    emit disconnected();
}

bool QClient::send( const QByteArray &buf )
{
    // Check for buffer validity
    if ( !buf.isEmpty() )
    {
        QMutexLocker lock( &_mutex );

        if ( _sock.isWritable() )
        {
            log( "[QClient] Sending Data: [%s]", buf.constData() );

            qint64 written = _sock.write( buf + PACKET_SEP );
            if ( written == -1 )
            {
                emit error( "Failed on write." );
                emit disconnected();

                return false;
            }

            return true;
        }
    }
    else
    {
        log( "Cannot write empty data." );
        return false;
    }

    // Notify listeners that the socket is no longer in a writable state
    emit error( "Socket is not in a writable state." );
    emit disconnected();

    return false;
}

bool QClient::isConnected() const
{
    QMutexLocker lock( const_cast<QMutex *>( &_mutex ) );

    return ( _sock.state() == QAbstractSocket::ConnectedState );
}

bool QClient::isConnecting() const
{
    QMutexLocker lock( const_cast<QMutex *>( &_mutex ) );

    return ( _sock.state() == QAbstractSocket::ConnectingState );
}

// ===========================================================================
// ==                        SOCKET SIGNAL HANDLING                         ==
// ===========================================================================

void QClient::onIncomingData()
{
    QByteArray data;

    // Get data from socket
    if ( _sock.isReadable() )
    {
        QMutexLocker lock( &_mutex );

        data = _sock.readAll();
    }

    if ( !data.isEmpty() )
    {
        // Data can be combined if the packets are small enough
        QList<QByteArray> packets = data.split( PACKET_SEP );

        foreach( const QByteArray &buf, packets )
        {
            if ( buf.isEmpty() ) continue;

            log( "[QClient] Recieving Data: [%s]", buf.constData() );

            // Notify that new data is ready
            emit incomingData( buf );
        }
    }
}

void QClient::onDisconnect()
{
    // Notify of disconnected state
    emit disconnected();
}

void QClient::onConnect()
{
    // Notify listeners of the host
    emit connected();
}

void QClient::onStateChanged( QAbstractSocket::SocketState socketState )
{
    // Notify of state change
    emit info( QSocketUtil::toString( socketState ) );
}

void QClient::onError( QAbstractSocket::SocketError errType )
{
    log( "[QClient] Error [%s]", qPrintable( QSocketUtil::toString( errType ) ) );

    // Notify listener of error
    emit error( QSocketUtil::toString( errType ) );
}

