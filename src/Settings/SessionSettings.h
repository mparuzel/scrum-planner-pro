/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * SessionSettings.h
 *
 *  Created on: May 20, 2013
 *      Author: mparuzel
 */

#ifndef SESSIONSETTINGS_H_
#define SESSIONSETTINGS_H_

// ===========================================================================

class QSessionSettings : public QObject
{
    Q_OBJECT

public:

    QSessionSettings( QSettings &settings );
    virtual ~QSessionSettings();

public slots:

    // Settings for Distributed Sessions
    QVariant getLastUserName() const;
    QVariant getLastHostIP() const;
    QVariant getLastPort() const;

    void setHost( const QVariant &ip, const QVariant &port );
    void setUser( const QVariant &user );

private:

    QSettings & _settingsRef;

};

// ===========================================================================

#endif
