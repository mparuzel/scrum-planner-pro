/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * SessionSettings.cpp
 *
 *  Created on: May 20, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "SessionSettings.h"
#include "Network/Defines.h"
#include "Utils/Log.h"

// ===========================================================================

// Constants (Keys)
const QString SESSION_GROUP     = "SessionInfo/";
const QString SESSION_HOSTNAME  = SESSION_GROUP + "HostName";  // String
const QString SESSION_PORT      = SESSION_GROUP + "Port";      // Int
const QString SESSION_USER      = SESSION_GROUP + "User";      // String

// ===========================================================================

QSessionSettings::QSessionSettings( QSettings &settings ) :
    _settingsRef( settings )
{
}

QSessionSettings::~QSessionSettings()
{
}

// ===========================================================================
// ==                             PUBLIC SLOTS                              ==
// ===========================================================================

QVariant QSessionSettings::getLastUserName() const
{
    return _settingsRef.value( SESSION_USER, "ScrumMaster" );
}

QVariant QSessionSettings::getLastHostIP() const
{
    return _settingsRef.value( SESSION_HOSTNAME, "127.0.0.1" );
}

QVariant QSessionSettings::getLastPort() const
{
    return _settingsRef.value( SESSION_PORT, BIND_PORT );
}

void QSessionSettings::setHost( const QVariant &ip, const QVariant &port )
{
    _settingsRef.setValue( SESSION_HOSTNAME, ip );
    _settingsRef.setValue( SESSION_PORT, port );
}

void QSessionSettings::setUser( const QVariant &user )
{
    _settingsRef.setValue( SESSION_USER, user );
}



