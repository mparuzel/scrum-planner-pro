/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * SettingsManager.cpp
 *
 *  Created on: May 17, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Settings/ProxySettings.h"
#include "Utils/Log.h"

// ===========================================================================

// Constants (Keys)
const QString PROXY_GROUP       = "ProxyInfo/";
const QString PROXY_ENABLED     = PROXY_GROUP + "Enabled";   // Bool
const QString PROXY_HOSTNAME    = PROXY_GROUP + "HostName";  // String
const QString PROXY_PORT        = PROXY_GROUP + "Port";      // Int
const QString PROXY_USER        = PROXY_GROUP + "User";      // String
const QString PROXY_PASSWORD    = PROXY_GROUP + "Password";  // String

// ===========================================================================

QProxySettings::QProxySettings( QSettings &settings ) :
    _settingsRef( settings )
{
    applySettings();
}

QProxySettings::~QProxySettings()
{
}

// ===========================================================================
// ==                     PROXY SETTINGS FUNCTIONALITY                      ==
// ===========================================================================

void QProxySettings::applySettings()
{
    // Apply settings globally
    QNetworkProxy::setApplicationProxy( getProxy() );
}

QNetworkProxy QProxySettings::getProxy() const
{
    QNetworkProxy out( QNetworkProxy::NoProxy );

    bool isUsingProxy = _settingsRef.value( PROXY_ENABLED, false ).toBool();
    if ( isUsingProxy )
    {
        out.setType( QNetworkProxy::Socks5Proxy );
        out.setHostName( _settingsRef.value( PROXY_HOSTNAME, "" ).toString() );
        out.setPort( _settingsRef.value( PROXY_PORT, 0 ).toInt() );
        out.setUser( _settingsRef.value( PROXY_USER, "" ).toString() );
        out.setPassword( _settingsRef.value( PROXY_PASSWORD, "" ).toString() );
    }

    return out;
}

// ===========================================================================
// ==                             PUBLIC SLOTS                              ==
// ===========================================================================

QVariant QProxySettings::isProxyEnabled() const
{
    return _settingsRef.value( PROXY_ENABLED, false );
}

QVariant QProxySettings::getProxyHostIP() const
{
    return _settingsRef.value( PROXY_HOSTNAME, "" );
}

QVariant QProxySettings::getProxyHostPort() const
{
    return _settingsRef.value( PROXY_PORT, 0 );
}

QVariant QProxySettings::getProxyUserName() const
{
    return _settingsRef.value( PROXY_USER, "" );
}

QVariant QProxySettings::getProxyPassword() const
{
    return _settingsRef.value( PROXY_PASSWORD, "" );
}

void QProxySettings::setProxy( bool enabled )
{
    _settingsRef.setValue( PROXY_ENABLED, enabled );
}

void QProxySettings::setCredentials( const QVariant &name, const QVariant &password )
{
    _settingsRef.setValue( PROXY_USER, name );
    _settingsRef.setValue( PROXY_PASSWORD, password );
}

void QProxySettings::setHost( const QVariant &ip, const QVariant &port )
{
    _settingsRef.setValue( PROXY_HOSTNAME, ip );
    _settingsRef.setValue( PROXY_PORT, port );
}
