/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * ProxySettings.h
 *
 *  Created on: May 17, 2013
 *      Author: mparuzel
 */

#ifndef PROXYSETTINGS_H_
#define PROXYSETTINGS_H_

// ===========================================================================

class QProxySettings : public QObject
{
    Q_OBJECT

public:

    QProxySettings( QSettings &settings );
    virtual ~QProxySettings();

    QNetworkProxy getProxy() const;

public slots:

    // Proxy Functionality
    void applySettings();

    // Accessors
    QVariant    isProxyEnabled() const;
    QVariant    getProxyHostPort() const;
    QVariant    getProxyHostIP() const;
    QVariant    getProxyUserName() const;
    QVariant    getProxyPassword() const;

    // Mutators
    void setProxy( bool enabled );
    void setCredentials( const QVariant &name, const QVariant &password );
    void setHost( const QVariant &ip, const QVariant &port );

private:

    QSettings &_settingsRef;

};

// ===========================================================================

#endif
