/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * InvokeManager.cpp
 *
 *  Created on: Apr 26, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Invocation/InvokeManager.h"
#include "Storage/ServerInfo.h"
#include "Storage/Deck.h"
#include "Utils/FileValidator.h"
#include "Utils/Log.h"

using namespace bb::system;

// ===========================================================================

// Target Name (Must be the same as in bar-descriptor.xml)
const QString g_DeckTarget = "com.bblabs.planningpoker.importdeck";
const QString g_ServerInfoTarget = "com.bblabs.planningpoker.serverinfo";
const QString g_ShareBody = "Please find the attached file to use with your Scrum Planner Pro application.";
const QString g_ShareSubject = "Scrum Planner Pro Deck";

// ===========================================================================

QInvokeManager::QInvokeManager() :
    _launchedByUser( false ),
    _invokedBySystem( false )
{
    // Listen to incoming invocation requests
    QObject::connect( &_invokeMgr, SIGNAL( invoked( const bb::system::InvokeRequest & ) ), this, SLOT( onInvoked( const bb::system::InvokeRequest & ) ) );

    // Determine who launched us
    switch ( _invokeMgr.startupMode() )
    {
        case ApplicationStartupMode::LaunchApplication:
            _launchedByUser = true;
            break;

        case ApplicationStartupMode::InvokeApplication:
            _invokedBySystem = true;
            break;

        default:
            log( "[QInvokeHandler] Invoked by unsupported means." );
            break;
    }

    log( "[QInvokeManager] Launched [%s]. Invoked [%s].", _launchedByUser ? "T" : "F", _invokedBySystem ? "T" : "F" );
}

QInvokeManager::~QInvokeManager()
{
}

// ===========================================================================
// ==                        INVOKE FUNCTIONALITY                          ==
// ===========================================================================

bool QInvokeManager::shareFile( const QVariant &filePath )
{
     QString fName = filePath.toString();

     if ( QFile::exists( fName ) )
     {
         InvokeRequest req;

         req.setAction( "bb.action.SHARE" );
         req.setUri( QUrl::fromLocalFile( fName ) );

         InvokeTargetReply *pRep = _invokeMgr.invoke( req );
         if ( pRep )
         {
             // Ensure that we are called when the invocation has finished
             QObject::connect( pRep, SIGNAL( finished() ), this, SLOT( onInvokeFinished() ) );

             pRep->setParent( this );
         }
         else
         {
             log( "[QInvokeManager] Failed to share file: %s.", qPrintable( fName ) );
         }

         return ( pRep != NULL );
     }

     log( "[QInvokeManager] File doesn't exist: %s.", qPrintable( fName ) );
     return false;
}

bool QInvokeManager::isLaunched() const
{
    return _launchedByUser;
}

bool QInvokeManager::isInvoked() const
{
    return _invokedBySystem;
}

// ===========================================================================
// ==                        INVOKE SIGNAL HANDLING                         ==
// ===========================================================================

void QInvokeManager::onInvoked( const InvokeRequest &req )
{
    QString filePath = req.uri().toString( QUrl::RemoveScheme ).remove( 0, 2 );
    QFile file( filePath );

    log( "[QInvokeHandler] Invoked by: %s.", qPrintable( req.target() ) );

    // Attempt to open the file
    if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        emit invokeError( "File [" + filePath + "] does not exist." );
        return;
    }

    QFileValidator fValid;

    // Check file contents against the XML schema
    if ( !fValid.isValid( file ) )
    {
        emit invokeError( "File contents are invalid or have been corrupted." );
        return;
    }

    // Rewind before reading data
    file.seek( 0 );

    // If invoked to import a deck
    if ( req.target() == g_DeckTarget )
    {
        QDeck deck;

        // Parse the XML and create a new deck
        if ( !deck.load( file.readAll() ) )
        {
            emit invokeError( "Cannot read the contents of invoked deck file." );
            return;
        }

        emit invokedDeck( deck.getName(), deck.getCards() );
    }
    else if ( req.target() == g_ServerInfoTarget )
    {
        QServerInfo svrInfo;

        // Parse the XML and create a new deck
        if ( !svrInfo.load( file.readAll() ) )
        {
            emit invokeError( "Cannot read the contents of invoked server info file." );
            return;
        }

        // If invoked for distributed communication
        emit invokedServerInfo( svrInfo.getHostIP().toString() );
    }
    else
    {
        emit invokeError( "Unknown invocation target." );
    }
}

void QInvokeManager::onInvokeFinished()
{
    // Grab the socket object
    InvokeTargetReply *pInv = qobject_cast<InvokeTargetReply *>( sender() );
    if ( pInv )
    {
        switch ( pInv->error() )
        {
            case InvokeReplyError::None:
                emit invokeSendSuccess();
                break;

            case InvokeReplyError::BadRequest:
                emit invokeError( "The request could not be parsed." );
                break;

            case InvokeReplyError::Internal:
                emit invokeError( "The invocation service failed as a result of an internal error." );
                break;

            case InvokeReplyError::NoTarget:
                emit invokeError( "No target was found." );
                break;

            case InvokeReplyError::TargetNotOwned:
                emit invokeError( "The requesting entity lacks sufficient privileges." );
                break;

            default:
                break;
        }

        // Delete the reply later on
        pInv->deleteLater();
    }
}


