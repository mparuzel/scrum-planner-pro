/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * InvokeManager.h
 *
 *  Created on: Apr 26, 2013
 *      Author: mparuzel
 */

#ifndef INVOKEMANAGER_H_
#define INVOKEMANAGER_H_

// ===========================================================================

class QServerInfo;
class QDeck;

// ===========================================================================

class QInvokeManager : public QObject
{
    Q_OBJECT

public:

    QInvokeManager();
    virtual ~QInvokeManager();

    // Accessors
    bool isLaunched() const;
    bool isInvoked() const;

signals:

    // Signals
    void invokedDeck( const QString &name, const QStringList &cards );
    void invokedServerInfo( const QString &ServerIP );
    void invokeError( const QString &err );
    void invokeSendSuccess();

public slots:

    // Funcitonality
    bool shareFile( const QVariant &filePath );

private slots:

    // Invocation handling
    void onInvoked( const bb::system::InvokeRequest &req );
    void onInvokeFinished();

private:

    // Invocation Status
    bb::system::InvokeManager   _invokeMgr;

    // Launch Source
    bool            _launchedByUser;
    bool            _invokedBySystem;

};

// ===========================================================================

#endif
