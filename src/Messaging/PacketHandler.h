/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * PacketHandler.h
 *
 *  Created on: Apr 30, 2013
 *      Author: mparuzel
 */

#ifndef MESSAGEHANDLER_H_
#define MESSAGEHANDLER_H_

// ===========================================================================

// Local Includes
#include "Storage/Deck.h"
#include "Messaging/PacketCommon.h"
#include "Messaging/Packet/Packet.h"

// ===========================================================================

class QPacketHandler : public QObject
{
    Q_OBJECT

public:

    QPacketHandler();
    virtual ~QPacketHandler();

    // Outgoing Packets (Admin Only)
    QByteArray createClearVotesPacket() const;
    QByteArray createSwitchDeckPacket( const QDeck &deck ) const;
    QByteArray createBootPacket( const QHostAddress &who ) const;
    QByteArray createAllVotesPacket( const QClientVotes &votes ) const;

    // Outgoing Informative Packets
    QByteArray createUserPacket( const QString &userName ) const;
    QByteArray createCastVotePacket( const QHostAddress &ip, const QString &deck, const QString &card ) const;

    // Outgoing Responses (Server Only)
    QByteArray createGreetPacket( const QHostAddress &who ) const;                          // Server Response to new connection
    QByteArray createAllUsersPacket( const QClientList &users ) const;                      // Server Response to new connection
    QByteArray createUserEnteredPacket( const QHostAddress &ip, const QString &name ) const;// Server Response to user entering
    QByteArray createUserLeftPacket( const QHostAddress &who ) const;                       // Server Response to user leaving

signals:

    // Incoming Server Packets
    void greet( const QHostAddress &yourAddr );                                             // On connected to server (Constructed By Server)
    void allUsers( const QClientList &clients );                                            // On connected to server (Constructed By Server)

    // Incoming Admin Packets
    void bootUser( const QHostAddress &who );                                               // On Admin request to boot user (Handled on Server Only)
    void switchDeck( const QDeck &deck );                                                   // On Admin switches deck (Admin Constructed, Relayed By Server)
    void allVotes( const QClientVotes &votes );                                             // On Admin receiving all votes (Admin Constructed, Relayed By Server)
    void clearVotes();                                                                      // On Admin clears votes (Admin Constructed, Relayed By Server)

    // Incoming User Packets
    void userEntered( const QHostAddress &ip, const QString &name );                        // On user enters (Constructed By Server)
    void userLeft( const QHostAddress &who );                                               // On user leaves (Constructed By Server)
    void userVotes( const QHostAddress &who, const QString &deck, const QString &card );    // On user votes (Client Constructed, Relayed By Server)
    void userConnected( const QHostAddress &who, const QString &name );                     // On user connect (Client connected, Prompts Server to send Greet and UserEntered)

    // Server Relay Packets
    void relayPacket( const QHostAddress &who, const QByteArray &data );                    // On client broadcast (Client Constructed, Unhandled by Server, Relayed)

public slots:

    // Incoming Packet Handling
    void onDataForClient( const QByteArray &data );
    void onDataForServer( const QHostAddress &who, const QByteArray &data );

private:

    typedef QMap<QString /* Tag */, QBasePacket *> QPacketMap;

    // All Packets that are being handled
    QPacketMap _parsers;

};

// ===========================================================================

#endif
