/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Packet.h
 *
 *  Created on: May 1, 2013
 *      Author: mparuzel
 */

#ifndef PACKET_H_
#define PACKET_H_

// ===========================================================================

class QBasePacket : public QObject
{
    Q_OBJECT

public:

    QBasePacket( const QString &vFile, const QString &tag );
    virtual ~QBasePacket();

    // Functions for Packet Serialization
    bool parse( const QByteArray &in );
    bool create( QByteArray &out );

    // Accessors
    bool isTagContained( const QByteArray &data ) const;
    QString getTag() const;

protected:

    virtual bool fromXML( QXmlStreamReader &stream ) = 0;
    virtual bool toXML( QXmlStreamWriter &stream ) const = 0;

private:

    QString             _tag;
    QXmlSchema          _schema;
    QXmlSchemaValidator _validator;

};

// ===========================================================================

template<const char TAG[]>
class QPacket : public QBasePacket
{
    class QBasePacketValue
    {
    public:

        QBasePacketValue() {};
        virtual ~QBasePacketValue() {};
    };

    template<class T>
    class QPacketValue : public QBasePacketValue,
                         public T
    {
    };

public:

    QPacket() : QBasePacket( QString( TAG ) + "Packet.xsd", TAG ) {}

    virtual ~QPacket()
    {
        foreach( QBasePacketValue *p, _map.values() )
        {
            delete p;
        }
    }

    // Mutator
    template<class T>
    T & val( const QString &id )
    {
        if ( !_map.contains( id ) )
        {
            _map[id] = static_cast<QBasePacketValue *>( new QPacketValue<T> );
        }

        return static_cast<T &>( *( static_cast<QPacketValue<T> *>( _map[id] ) ) );
    }

    // Accessor
    template<class T>
    const T & val( const QString &id ) const
    {
        return static_cast<const T &>( *( static_cast<QPacketValue<T> *>( _map[id] ) ) );
    }

private:

    // Implementation of QBasePacket Functions
    bool fromXML( QXmlStreamReader &stream );
    bool toXML( QXmlStreamWriter &stream ) const;

private:

    QMap<QString, QBasePacketValue *> _map;

};

// ===========================================================================

#endif
