/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * PacketMacros.h
 *
 *  Created on: May 5, 2013
 *      Author: mparuzel
 */

#ifndef PACKETMACROS_H_
#define PACKETMACROS_H_

// ===========================================================================

// Macros To tidy up Code
#define _ip         val<QHostAddress>( "ip" )
#define _deck       val<QDeck>( "deck" )
#define _deckName   val<QString>( "deckName" )
#define _votes      val<QClientVotes>( "votes" )
#define _card       val<QString>( "card" )
#define _name       val<QString>( "name" )
#define _users      val<QClientList>( "users" )

// ===========================================================================

#endif /* PACKETMACROS_H_ */
