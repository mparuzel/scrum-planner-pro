/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * PacketDefinitions.h
 *
 *  Created on: May 2, 2013
 *      Author: mparuzel
 */

#ifndef PACKETDEFINITIONS_H_
#define PACKETDEFINITIONS_H_

// ===========================================================================

#include "Storage/Deck.h"
#include "Messaging/Packet/Packet.h"
#include "Messaging/PacketCommon.h"

// ===========================================================================

// Packet Tag names
extern const char g_GreetTag[];
extern const char g_CardTag[];
extern const char g_SwitchDeckTag[];
extern const char g_BootTag[];
extern const char g_AllVotesTag[];
extern const char g_VoteTag[];
extern const char g_UserTag[];
extern const char g_AllUserTag[];
extern const char g_UserEnteredTag[];
extern const char g_UserLeftTag[];
extern const char g_ClearVotesTag[];

// Compiler will complain if there is a duplication of template parameters
typedef QPacket<g_UserTag>          QUserPacket;
typedef QPacket<g_GreetTag>         QGreetPacket;
typedef QPacket<g_SwitchDeckTag>    QSwitchDeckPacket;
typedef QPacket<g_BootTag>          QBootPacket;
typedef QPacket<g_AllVotesTag>      QAllVotesPacket;
typedef QPacket<g_VoteTag>          QCastVotePacket;
typedef QPacket<g_AllUserTag>       QAllUsersPacket;
typedef QPacket<g_UserEnteredTag>   QUserEnteredPacket;
typedef QPacket<g_UserLeftTag>      QUserLeftPacket;
typedef QPacket<g_ClearVotesTag>    QClearVotesPacket;

// ===========================================================================

#endif
