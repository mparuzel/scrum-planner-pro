/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Packet.cpp
 *
 *  Created on: May 1, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Storage/Deck.h"
#include "Messaging/Packet/Packet.h"
#include "Utils/Log.h"

// ===========================================================================

// Path to XSD
const QString g_XSDAssetPath    = "/app/native/assets/xsd/";

// ===========================================================================

QBasePacket::QBasePacket( const QString &vFile, const QString &tag ) :
    _tag( tag )
{
    // Load the schema
    if ( !_schema.load( QDir::currentPath() + g_XSDAssetPath + vFile ) ||
         !_schema.isValid() )
    {
        log( "[QBasePacket] Cannot load schema file [%s].", qPrintable( vFile ) );
    }
    else
    {
        _validator.setSchema( _schema );
    }
}

QBasePacket::~QBasePacket()
{
}

bool QBasePacket::parse( const QByteArray &in )
{
    // Sanity check
    if ( !_schema.isValid() )
    {
        log( "[QBasePacket] Schema is invalid." );
        return false;
    }

    // Check to see if the data belongs to this packet
    if ( isTagContained( in ) )
    {
        // Validate the XML against the XSD
        if ( !_validator.validate( in ) )
        {
            log( "[QBasePacket] Could not validate XML." );
            return false;
        }

        QXmlStreamReader reader( in );

        while ( !reader.atEnd() )
        {
            reader.readNext();

            // Ignore non element types
            if ( !reader.isStartElement() ) continue;

            // Parse element
            if ( !fromXML( reader ) )
            {
                log( "[QBasePacket] Cannot parse reader tag [%s].", qPrintable( reader.name().toString() ) );
                return false;
            }
        }

        return !reader.hasError();
    }
    else
    {
        log( "[QBasePacket] Data being parsed doesn't contain tag [%s].", qPrintable( _tag ) );
    }

    return false;
}

bool QBasePacket::create( QByteArray &out )
{
    QXmlStreamWriter stream( &out );

    stream.setAutoFormatting( true );
    stream.writeStartDocument();

    // Create XML and serialize it to the buffer
    bool ret = toXML( stream );

    stream.writeEndDocument();

    return ret && !stream.hasError();
}

bool QBasePacket::isTagContained( const QByteArray& data ) const
{
    // Check to see if the data belongs to this packet
    return data.contains( _tag.toLocal8Bit() );
}

QString QBasePacket::getTag() const
{
    return _tag;
}


