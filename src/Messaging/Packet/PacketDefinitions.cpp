/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * PacketDefinitions.cpp
 *
 *  Created on: May 2, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// ===========================================================================

// Local Includes
#include "Messaging/Packet/PacketDefinitions.h"
#include "Messaging/Packet/PacketMacros.h"
#include "Utils/Log.h"

// ===========================================================================

// Tags
const char g_GreetTag[]        = "Greet";
const char g_CardTag[]         = "Card";
const char g_SwitchDeckTag[]   = "SwitchDeck";
const char g_BootTag[]         = "Boot";
const char g_AllVotesTag[]     = "AllVotes";
const char g_VoteTag[]         = "Vote";
const char g_UserTag[]         = "User";
const char g_AllUserTag[]      = "AllUsers";
const char g_UserEnteredTag[]  = "UserEntered";
const char g_UserLeftTag[]     = "UserLeft";
const char g_ClearVotesTag[]   = "ClearVotes";

// ===========================================================================

// Example:
//      <User name="ScrumMaster" />
//

template<>
bool QUserPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_UserTag )
    {
        QXmlStreamAttributes attr = stream.attributes();

        // Extract the attributes
        _name = attr.value( "name" ).toString();

        return !_name.isEmpty();
    }

    return false;
}

template<>
bool QUserPacket::toXML( QXmlStreamWriter &stream ) const
{
    // Sanity
    if ( _name.isEmpty() )
    {
        log( "[QUserPacket] User name is empty." );
        return false;
    }

    // Write the tag
    stream.writeStartElement( g_UserTag );
    stream.writeAttribute( "name", _name );
    stream.writeEndElement();

    return !stream.hasError();
}

// ===========================================================================

// Example:
//      <Greet ip="192.168.1.1" admin="false" />
//

template<>
bool QGreetPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_GreetTag )
    {
        QXmlStreamAttributes attr = stream.attributes();

        // Extract the attributes
        QString ip = attr.value( "ip" ).toString();
        if ( _ip.setAddress( ip ) )
        {
            return true;
        }

        log( "[QGreetPacket] IP Address [%s] is Invalid.", qPrintable( ip ) );
        return !_ip.isNull();
    }

    return false;
}

template<>
bool QGreetPacket::toXML( QXmlStreamWriter &stream ) const
{
    // Sanity
    if ( _ip.isNull() )
    {
        log( "[QGreetPacket] IP Address is NULL." );
        return false;
    }

    // Write the tag
    stream.writeStartElement( g_GreetTag );
    stream.writeAttribute( "ip", _ip.toString() );
    stream.writeEndElement();

    return !stream.hasError();
}

// ===========================================================================

// Example:
//      <SwitchDeck name="Deckname" >
//        <Card>1</Card>
//        <Card>2</Card>
//        <Card>6</Card>
//        <Card>9</Card>
//        <Card>12</Card>
//      </SwitchDeck>
//

template<>
bool QSwitchDeckPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_SwitchDeckTag )
    {
        QXmlStreamAttributes attr = stream.attributes();
        QString name = attr.value( "name" ).toString();

        if ( !name.isEmpty() )
        {
            // Clear the deck of any persistent data
            _deck.clear();

            // Extract the attributes
            _deck.setName( name );
            return true;
        }

        log( "[QSwitchDeckPacket] Deck name is empty." );
    }
    else if ( stream.name() == g_CardTag )
    {
        QString card = stream.readElementText();
        if ( !card.isEmpty() )
        {
            // Add card by name
            _deck.addCard( card );
            return true;
        }

        log( "[QSwitchDeckPacket] Card name is empty." );
    }

    return false;
}

template<>
bool QSwitchDeckPacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( _deck.isValid() )
    {
        stream.writeStartElement( g_SwitchDeckTag );
        stream.writeAttribute( "name", _deck.getName() );

        QStringList cards = _deck.getCards();

        // Write each card separately
        foreach ( const QString &card, cards )
        {
            stream.writeStartElement( g_CardTag );
            stream.writeCharacters( card );
            stream.writeEndElement();
        }

        stream.writeEndElement();

        return true;
    }

    log( "[QSwitchDeckPacket] Deck is not valid." );
    return false;
}

// ===========================================================================

// Example:
//      <Boot ip="192.168.1.1" />
//

template<>
bool QBootPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_BootTag )
    {
        QXmlStreamAttributes attr = stream.attributes();
        QString ip = attr.value( "ip" ).toString();

        // Extract and set attribute
        if ( _ip.setAddress( ip ) )
        {
            return true;
        }

        log( "[QBootPacket] Cannot set IP [%s].", qPrintable( ip ) );
    }

    return false;
}

template<>
bool QBootPacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( !_ip.isNull() )
    {
        // Write the tag
        stream.writeStartElement( g_BootTag );
        stream.writeAttribute( "ip", _ip.toString() );
        stream.writeEndElement();

        return !stream.hasError();
    }

    log( "[QBootPacket] Cannot compose XML, address is NULL." );
    return false;
}

// ===========================================================================

// Example:
//      <AllVotes>
//        <Vote who="192.168.1.1">1</Vote>
//        <Vote who="192.168.1.2">6</Vote>
//        <Vote who="192.168.1.3">6</Vote>
//        <Vote who="192.168.1.4">1</Vote>
//      </AllVotes>
//

template<>
bool QAllVotesPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_AllVotesTag )
    {
        // Clear any extraneous data
        _votes.clear();

        return true;
    }
    else if ( stream.name() == g_VoteTag )
    {
        QXmlStreamAttributes attr = stream.attributes();

        // Get the values from this tag
        QString who = attr.value( "who" ).toString();
        QString card = stream.readElementText();

        if ( !who.isEmpty() &&
             !card.isEmpty() )
        {
            _votes[who] = card;

            return true;
        }

        log( "[QClientVotesPacket] IP [%s] or Card [%s] is invalid.", qPrintable( who ), qPrintable( card ) );
    }

    return false;
}

template<>
bool QAllVotesPacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( !_votes.isEmpty() )
    {
        stream.writeStartElement( g_AllVotesTag );

        // Write the list of cards
        for ( QClientVotes::const_iterator it = _votes.begin();
              it != _votes.end();
              it++ )
        {
            // Sanity check
            if ( it.key().isEmpty() )
            {
                log( "[QClientVotesPacket] Address is Null." );
                return false;
            }

            // Write cards
            stream.writeStartElement( g_VoteTag );
            stream.writeAttribute( "who", it.key() );
            stream.writeCharacters( it.value() );
            stream.writeEndElement();
        }

        stream.writeEndElement();

        return !stream.hasError();
    }

    log( "[QClientVotesPacket] Vote list is empty." );
    return false;
}

// ===========================================================================

// Example:
//      <Vote who="192.168.1.1" deckname="Deckname">1</Vote>
//

template<>
bool QCastVotePacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_VoteTag )
    {
        QXmlStreamAttributes attr = stream.attributes();

        // Get the values from this tag
        QString who = attr.value( "who" ).toString();
        QString deckName = attr.value( "deckName" ).toString();
        QString card = stream.readElementText();

        if ( !who.isEmpty() &&
             !card.isEmpty() &&
             !deckName.isEmpty() )
        {
            _card = card;
            _deckName = deckName;

            return _ip.setAddress( who );
        }

        log( "[QCastVotePacket] IP [%s] or Card [%s] is invalid.", qPrintable( who ), qPrintable( card ) );
    }

    return false;
}

template<>
bool QCastVotePacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( !_ip.isNull() &&
         !_card.isEmpty() &&
         !_deckName.isEmpty() )
    {
        // Write card
        stream.writeStartElement( g_VoteTag );
        stream.writeAttribute( "who", _ip.toString() );
        stream.writeAttribute( "deckName", _deckName );
        stream.writeCharacters( _card );
        stream.writeEndElement();

        return !stream.hasError();
    }

    log( "[QCastVotePacket] Cannot compose XML, IP [%s], Deck Name [%s], or Card [%s] is invalid.", qPrintable( _ip.toString() ), qPrintable( _deckName ), qPrintable( _card ) );
    return false;
}

// ===========================================================================

// Example:
//      <AllUsers>
//        <User name="ScrumMaster" ip="192.168.1.1" />
//        <User name="AAAA" ip="192.168.1.2" />
//        <User name="BBBB" ip="192.168.1.5" />
//        <User name="CCCC" ip="192.168.1.8" />
//      </AllUsers>
//

template<>
bool QAllUsersPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_AllUserTag )
    {
        // Clear users before appending user list
        _users.clear();

        return true;
    }
    else if ( stream.name() == g_UserTag )
    {
        QXmlStreamAttributes attr = stream.attributes();

        // Get the values from this tag
        QString name = attr.value( "name" ).toString();
        QString ip = attr.value( "ip" ).toString();

        if ( !name.isEmpty() &&
             !ip.isEmpty() )
        {
            _users[ip] = name;
            return true;
        }

        log( "[QAllUsersPacket] IP [%s] or User [%s] is invalid.", qPrintable( ip ), qPrintable( name ) );
    }

    return false;
}

template<>
bool QAllUsersPacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( !_users.isEmpty() )
    {
        stream.writeStartElement( g_AllUserTag );

        // Write the list of cards
        for ( QClientList::const_iterator it = _users.begin();
              it != _users.end();
              it++ )
        {
            // Sanity check on IP
            if ( it.key().isEmpty() )
            {
                log( "[QAllUsersPacket] IP Address is empty." );
                return false;
            }

            // Write user info
            stream.writeStartElement( g_UserTag );
            stream.writeAttribute( "name", it.value() );
            stream.writeAttribute( "ip", it.key() );
            stream.writeEndElement();
        }

        stream.writeEndElement();

        return !stream.hasError();
    }

    log( "[QAllUsersPacket] Cannot write empty user list." );
    return false;
}

// ===========================================================================

// Example:
//      <UserEntered name="ScrumMaster" ip="192.168.1.1" />
//

template<>
bool QUserEnteredPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_UserEnteredTag )
    {
        QXmlStreamAttributes attr = stream.attributes();

        // Get the values from this tag
        QString name = attr.value( "name" ).toString();
        QString ip = attr.value( "ip" ).toString();

        if ( !name.isEmpty() &&
             !ip.isEmpty() )
        {
            _name = name;

            return _ip.setAddress( ip );
        }

        log( "[QUserEnteredPacket] IP [%s] or User [%s] is invalid.", qPrintable( ip ), qPrintable( name ) );
    }

    return false;
}

template<>
bool QUserEnteredPacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( !_name.isEmpty() &&
         !_ip.isNull() )
    {
        // Write user data
        stream.writeStartElement( g_UserEnteredTag );
        stream.writeAttribute( "name", _name );
        stream.writeAttribute( "ip", _ip.toString() );
        stream.writeEndElement();

        return !stream.hasError();
    }

    log( "[QUserEnteredPacket] Cannot write IP [%s] or User [%s].", qPrintable( _name ), qPrintable( _ip.toString() ) );
    return false;
}

// ===========================================================================

// Example:
//      <UserLeft ip="192.168.1.1" />
//

template<>
bool QUserLeftPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_UserLeftTag )
    {
        QXmlStreamAttributes attr = stream.attributes();
        QString ip = attr.value( "ip" ).toString();

        // Get the values from this tag
        if ( _ip.setAddress( ip ) )
        {
            return true;
        }

        log( "[QUserLeftPacket] IP [%s] is invalid.", qPrintable( ip ) );
    }

    return false;
}

template<>
bool QUserLeftPacket::toXML( QXmlStreamWriter &stream ) const
{
    if ( !_ip.isNull() )
    {
        // Write user data
        stream.writeStartElement( g_UserLeftTag );
        stream.writeAttribute( "ip", _ip.toString() );
        stream.writeEndElement();

        return !stream.hasError();
    }

    log( "[QUserLeftPacket] Cannot write, IP [%s] is invalid.", qPrintable( _ip.toString() ) );
    return false;
}

// ===========================================================================

// Example:
//      <ClearVotes />
//

template<>
bool QClearVotesPacket::fromXML( QXmlStreamReader &stream )
{
    if ( stream.name() == g_ClearVotesTag )
    {
        return true;
    }

    return false;
}

template<>
bool QClearVotesPacket::toXML( QXmlStreamWriter &stream ) const
{
    // Write the tag
    stream.writeStartElement( g_ClearVotesTag );
    stream.writeEndElement();

    return !stream.hasError();
}
