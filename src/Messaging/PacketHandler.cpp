/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * PacketHandler.cpp
 *
 *  Created on: Apr 30, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "Messaging/PacketHandler.h"
#include "Messaging/Packet/PacketDefinitions.h"
#include "Messaging/Packet/PacketMacros.h"
#include "Utils/Log.h"

// ===========================================================================

QPacketHandler::QPacketHandler()
{
    // Create a list of handlers to parse messages
    _parsers[g_ClearVotesTag]   = new QClearVotesPacket;
    _parsers[g_SwitchDeckTag]   = new QSwitchDeckPacket;
    _parsers[g_BootTag]         = new QBootPacket;
    _parsers[g_AllVotesTag]     = new QAllVotesPacket;
    _parsers[g_VoteTag]         = new QCastVotePacket;
    _parsers[g_GreetTag]        = new QGreetPacket;
    _parsers[g_AllUserTag]      = new QAllUsersPacket;
    _parsers[g_UserEnteredTag]  = new QUserEnteredPacket;
    _parsers[g_UserLeftTag]     = new QUserLeftPacket;
    _parsers[g_UserTag]         = new QUserPacket;

    // Ownership of lifetime is managed by this class
    foreach( QBasePacket *pPacket, _parsers.values() )
    {
        pPacket->setParent( this );
    }
}

QPacketHandler::~QPacketHandler()
{
}

void QPacketHandler::onDataForClient( const QByteArray &data )
{
    // This shouldn't happen, but check anyway
    if ( data.isEmpty() )
    {
        log( "[QPacketHandler] Data for client is empty." );
        return;
    }

    // These tags are only relevant to the client
    QString tagsToParse[] = { g_ClearVotesTag,
                              g_GreetTag,
                              g_AllUserTag,
                              g_SwitchDeckTag,
                              g_AllVotesTag,
                              g_UserEnteredTag,
                              g_UserLeftTag,
                              g_VoteTag };

    for ( size_t i = 0; i < sizeof( tagsToParse ) / sizeof( tagsToParse[0] ); ++i )
    {
        QBasePacket *p = _parsers[tagsToParse[i]];

        // Ignore packets that cannot handle this data
        if ( !p->isTagContained( data ) ) continue;

        // Bail on failure
        if ( !p->parse( data ) ) return;

        QString tag = p->getTag();

        // Emit signals for client to handle
        if ( tag == g_ClearVotesTag )   emit clearVotes();
        if ( tag == g_GreetTag )        emit greet( static_cast<QGreetPacket *>( p )->_ip );
        if ( tag == g_AllUserTag )      emit allUsers( static_cast<QAllUsersPacket *>( p )->_users );
        if ( tag == g_SwitchDeckTag )   emit switchDeck( static_cast<QSwitchDeckPacket *>( p )->_deck );
        if ( tag == g_AllVotesTag )     emit allVotes( static_cast<QAllVotesPacket *>( p )->_votes );
        if ( tag == g_UserEnteredTag )  emit userEntered( static_cast<QGreetPacket *>( p )->_ip, static_cast<QUserEnteredPacket *>( p )->_name );
        if ( tag == g_UserLeftTag )     emit userLeft( static_cast<QUserLeftPacket *>( p )->_ip );
        if ( tag == g_VoteTag )         emit userVotes( static_cast<QCastVotePacket *>( p )->_ip, static_cast<QCastVotePacket *>( p )->_deckName, static_cast<QCastVotePacket *>( p )->_card );

        return;
    }

    log( "[QPacketHandler] Could not find a packet to parse Client data." );
}

void QPacketHandler::onDataForServer( const QHostAddress &who, const QByteArray &data )
{
    // This shouldn't happen, but check anyway
    if ( data.isEmpty() )
    {
        log( "[QPacketHandler] Data for server is empty." );
        return;
    }

    // Tags only relevant to server
    QString tagsToParse[] = { g_BootTag,
                              g_UserTag };

    for ( size_t i = 0; i < sizeof( tagsToParse ) / sizeof( tagsToParse[0] ); ++i )
    {
        QBasePacket *p = _parsers[tagsToParse[i]];

        // Ignore packets that cannot handle this data
        if ( !p->isTagContained( data ) ) continue;

        // Bail on failure
        if ( !p->parse( data ) ) return;

        QString tag = p->getTag();

        // Emit signals for server to handle
        if ( tag == g_BootTag ) emit bootUser( static_cast<QBootPacket *>( p )->_ip );
        if ( tag == g_UserTag ) emit userConnected( who, static_cast<QUserPacket *>( p )->_name );

        return;
    }

    // Unhandled data gets relayed
    emit relayPacket( who, data );
}

QByteArray QPacketHandler::createClearVotesPacket() const
{
    QByteArray data;
    QClearVotesPacket packet;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create ClearVotes packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createSwitchDeckPacket( const QDeck &deck ) const
{
    QByteArray data;
    QSwitchDeckPacket packet;

    // Set packet data
    packet._deck = deck;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create SwitchDeck packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createBootPacket( const QHostAddress &who ) const
{
    QByteArray data;
    QBootPacket packet;

    // Set packet data
    packet._ip = who;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create Boot packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createAllVotesPacket( const QClientVotes &votes ) const
{
    QByteArray data;
    QAllVotesPacket packet;

    // Set packet data
    packet._votes = votes;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create ClientVotes packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createCastVotePacket( const QHostAddress &ip, const QString &deck, const QString &card ) const
{
    QByteArray data;
    QCastVotePacket packet;

    // Set packet data
    packet._ip = ip;
    packet._card = card;
    packet._deckName = deck;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create CastVotes packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createGreetPacket( const QHostAddress &who ) const
{
    QByteArray data;
    QGreetPacket packet;

    // Set packet data
    packet._ip = who;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create Greet packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createAllUsersPacket( const QClientList &users ) const
{
    QByteArray data;
    QAllUsersPacket packet;

    // Set packet data
    packet._users = users;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create AllUsers packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createUserEnteredPacket( const QHostAddress &ip, const QString &name ) const
{
    QByteArray data;
    QUserEnteredPacket packet;

    // Set packet data
    packet._name = name;
    packet._ip = ip;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create UserEntered packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createUserPacket( const QString &userName ) const
{
    QByteArray data;
    QUserPacket packet;

    // Set packet data
    packet._name = userName;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create User packet." );

    // Return empty array
    return QByteArray();
}

QByteArray QPacketHandler::createUserLeftPacket( const QHostAddress &who ) const
{
    QByteArray data;
    QUserLeftPacket packet;

    // Set packet data
    packet._ip = who;

    // Create xml packet
    if ( packet.create( data ) )
    {
        return data;
    }

    log( "[QPacketHandler] Failed to create UserLeft packet." );

    // Return empty array
    return QByteArray();
}


