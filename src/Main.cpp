/*
 * Copyright (C) 2013-2014 Mark Paruzel <mark@markparuzel.com>
 * 
 * This file is part of Scrum Planner Pro (aka. Planning Poker).
 *
 * Scrum Planner Pro is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version.
 *
 * Scrum Planner Pro is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License 
 * along with Scrum Planner Pro. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ===========================================================================
 *
 * Main.cpp
 *
 *  Created on: Apr 19, 2013
 *      Author: mparuzel
 */

// System Includes
#include "Precompiled.h"

// Local Includes
#include "App.h"
#include "Utils/Log.h"

// ===========================================================================

Q_DECL_EXPORT int main( int argc, char **argv )
{
    // This is where the server is started etc
    bb::cascades::Application app( argc, argv );
    QTranslator translator;
    QString filename = QString( "PlanningPoker_%1" ).arg( QLocale().name() );

    // Localization support
    if ( translator.load( filename, "app/native/qm") )
    {
        app.installTranslator( &translator );
    }

    // Initialize Scrum Planner Pro (Lifetime managed by app)
    PlanningPokerApp *pPPApp = new PlanningPokerApp( &app );
    if ( pPPApp )
    {
        pPPApp->init( app );
    }

    // We complete the transaction started in the app constructor and start the client event loop here
    return Application::exec();
}
