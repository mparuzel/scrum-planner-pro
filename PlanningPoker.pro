APP_NAME = PlanningPoker

# Include first
include(config.pri)

# Override afterward
CONFIG += qt warn_on debug_and_release cascades10 precompiled_header

LIBS += -lbbcascadespickers 
LIBS += -lbbpim
LIBS += -lbbplatform
LIBS += -lbbsystem
LIBS += -lbb

device {
    CONFIG(debug, debug|release) {
        # Device-Debug custom configuration
    }

    CONFIG(release, debug|release) {
        # Device-Release custom configuration
    }
}

simulator {
    CONFIG(debug, debug|release) {
        # Simulator-Debug custom configuration
    }
}
